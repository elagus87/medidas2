
class bandwidth:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def ndb(*n):
		if len(n) == 0:
			inst.query("CALCulate:BANDwidth:NDB?")
		else:
			inst.query("CALCulate:BANDwidth:NDB " +str(n[0]))
	def result_2(*n):
		if len(n) == 0:
			inst.query("CALCulate:BANDwidth:RESult??")
		else:
			inst.query("CALCulate:BANDwidth:RESult? " +str(n[0]))

class lline:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def all_delete(*n):
		if len(n) == 0:
			inst.query("CALCulate:LLINe:ALL:DELete?")
		else:
			inst.query("CALCulate:LLINe:ALL:DELete " +str(n[0]))
	def control_domain(*n):
		if len(n) == 0:
			inst.query("CALCulate:LLINe:CONTrol:DOMain?")
		else:
			inst.query("CALCulate:LLINe:CONTrol:DOMain " +str(n[0]))
	def fail_2(*n):
		if len(n) == 0:
			inst.query("CALCulate:LLINe:FAIL??")
		else:
			inst.query("CALCulate:LLINe:FAIL? " +str(n[0]))
	def fail_ratio_2(*n):
		if len(n) == 0:
			inst.query("CALCulate:LLINe:FAIL:RATIo??")
		else:
			inst.query("CALCulate:LLINe:FAIL:RATIo? " +str(n[0]))
	def fail_stop_state(*n):
		if len(n) == 0:
			inst.query("CALCulate:LLINe:FAIL:STOP:STATe?")
		else:
			inst.query("CALCulate:LLINe:FAIL:STOP:STATe " +str(n[0]))
	def control_interpolate_type(*n):
		if len(n) == 0:
			inst.query("CALCulate:LLINe"+str(op)+":CONTrol:INTerpolate:TYPE?")
		else:
			inst.query("CALCulate:LLINe"+str(op)+":CONTrol:INTerpolate:TYPE " +str(n[0]))
	def data(*n):
		if len(n) == 0:
			inst.query("CALCulate:LLINe"+str(op)+":DATA?")
		else:
			inst.query("CALCulate:LLINe"+str(op)+":DATA " +str(n[0]))
	def data_merge(*n):
		if len(n) == 0:
			inst.query("CALCulate:LLINe"+str(op)+":DATA:MERGe?")
		else:
			inst.query("CALCulate:LLINe"+str(op)+":DATA:MERGe " +str(n[0]))
	def delete(*n):
		if len(n) == 0:
			inst.query("CALCulate:LLINe"+str(op)+":DELete?")
		else:
			inst.query("CALCulate:LLINe"+str(op)+":DELete " +str(n[0]))
	def relampt_state(*n):
		if len(n) == 0:
			inst.query("CALCulate:LLINe"+str(op)+":RELAmpt:STATe?")
		else:
			inst.query("CALCulate:LLINe"+str(op)+":RELAmpt:STATe " +str(n[0]))
	def relfreq_state(*n):
		if len(n) == 0:
			inst.query("CALCulate:LLINe"+str(op)+":RELFreq:STATe?")
		else:
			inst.query("CALCulate:LLINe"+str(op)+":RELFreq:STATe " +str(n[0]))
	def state(*n):
		if len(n) == 0:
			inst.query("CALCulate:LLINe"+str(op)+":STATe?")
		else:
			inst.query("CALCulate:LLINe"+str(op)+":STATe " +str(n[0]))

class marker:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	
    #revisada a mano
	def aoff():
		inst.write("CALCulate:MARKer:AOFF")
        
    #revisada a mano
    def fcount_resolution(*n):
		if len(n) == 1:
			return inst.query("CALCulate:MARKer:FCOunt:RESolution?")
		else:
			inst.write("CALCulate:MARKer:FCOunt:RESolution " +str(n[0]))
            
    #revisada a mano
    def fcount_resolution_auto():
        if len(n) == 1:
            inst.write("CALCulate:MARKer:FCOunt:RESolution:AUTO?")
        else:
			inst.write("CALCulate:MARKer:FCOunt:RESolution:AUTO " +str(n[0]))
            
	def fcount_x_2(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer:FCOunt:X??")
		else:
			inst.query("CALCulate:MARKer:FCOunt:X? " +str(n[0]))
	def fcount_state(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer:FCOunt:STATe?")
		else:
			inst.query("CALCulate:MARKer:FCOunt:STATe " +str(n[0]))
            
    #revisada a mano
    #marcador n estado s
	def cpeak_state(*n,*s):
        if len(n) == 1:
            print("uso: CALCulate:MARKer<n>:CPEak[:STATe]<? o 0,1>")
		elif len(s) == 1:
			inst.query("CALCulate:MARKer"+str(n[0])+":CPEak[:STATe]?")
		else:
			inst.write("CALCulate:MARKer"+str(n[0])+":CPEak[:STATe] " +str(s[1]))
            
         
	def delta_set_center(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":DELTa:SET:CENTer?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":DELTa:SET:CENTer " +str(n[0]))
	def delta_set_span(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":DELTa:SET:SPAN?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":DELTa:SET:SPAN " +str(n[0]))
	def function(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":FUNCtion?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":FUNCtion " +str(n[0]))
	def maximum_left(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":MAXimum:LEFT?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":MAXimum:LEFT " +str(n[0]))
	def maximum_max(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":MAXimum:MAX?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":MAXimum:MAX " +str(n[0]))
	def maximum_next(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":MAXimum:NEXT?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":MAXimum:NEXT " +str(n[0]))
	def maximum_right(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":MAXimum:RIGHt?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":MAXimum:RIGHt " +str(n[0]))
	def minimum(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":MINimum?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":MINimum " +str(n[0]))
	def mode(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":MODE?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":MODE " +str(n[0]))
	def peak_excursion(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":PEAK:EXCursion?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":PEAK:EXCursion " +str(n[0]))
	def peak_search_mode(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":PEAK:SEARch:MODE?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":PEAK:SEARch:MODE " +str(n[0]))
	def peak_set_cf(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":PEAK:SET:CF?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":PEAK:SET:CF " +str(n[0]))
	def peak_threshold(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":PEAK:THReshold?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":PEAK:THReshold " +str(n[0]))
	def ptpeak(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":PTPeak?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":PTPeak " +str(n[0]))
	def set_center(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":SET:CENTer?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":SET:CENTer " +str(n[0]))
	def set_rlevel(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":SET:RLEVel?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":SET:RLEVel " +str(n[0]))
	def set_start(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":SET:STARt?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":SET:STARt " +str(n[0]))
	def set_step(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":SET:STEP?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":SET:STEP " +str(n[0]))
	def set_stop(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":SET:STOP?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":SET:STOP " +str(n[0]))
            
    #revisada a mano
    #marcador n estado s
	def state(*n,*s):
        if len(n) == 1:
            print("uso: CALCulate:MARKer<n>:STATe 0,1")
		elif len(s) == 1:
			inst.query("CALCulate:MARKer"+str(n[0])+":STATe?")
		else:
			inst.write("CALCulate:MARKer"+str(n[0])+":STATe " +str(s[1]))
	
	def trace(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":TRACe?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":TRACe " +str(n[0]))
	def trace_auto(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":TRACe:AUTO?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":TRACe:AUTO " +str(n[0]))
	def vsrefl_2(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":VSRefl??")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":VSRefl? " +str(n[0]))
	def vsvalue_2(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":VSValue??")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":VSValue? " +str(n[0]))
            
    #revisada a mano
    #marcador n estado s
	def x(*n,*param):
        if len(n):
            print("uso: CALCulate:MARKer<n>:X parametro")
		elif len(param) == 1:
			inst.query("CALCulate:MARKer"+str(n)+":X?")
		else:
			inst.write("CALCulate:MARKer"+str(n)+":X " +str(param[1]))
	
	def x_center(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":X:CENTer?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":X:CENTer " +str(n[0]))
	def x_position(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":X:POSition?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":X:POSition " +str(n[0]))
	def x_position_center(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":X:POSition:CENTer?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":X:POSition:CENTer " +str(n[0]))
	def x_position_span(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":X:POSition:SPAN?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":X:POSition:SPAN " +str(n[0]))
	def x_position_start(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":X:POSition:STARt?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":X:POSition:STARt " +str(n[0]))
	def x_position_stop(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":X:POSition:STOP?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":X:POSition:STOP " +str(n[0]))
	def x_readout(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":X:READout?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":X:READout " +str(n[0]))
	def x_span(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":X:SPAN?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":X:SPAN " +str(n[0]))
	def x_start(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":X:STARt?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":X:STARt " +str(n[0]))
	def x_stop(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer"+str(op)+":X:STOP?")
		else:
			inst.query("CALCulate:MARKer"+str(op)+":X:STOP " +str(n[0]))
	
    #revisada a mano
    #marcador n estado s
    def y(*n):
		if len(n) == 1:
			print("uso: CALCulate:MARKer<n><:Y?")
		else:
			inst.query("CALCulate:MARKer"+str(n[0])+":Y?")
            
	def table_state(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer:TABLe:STATe?")
		else:
			inst.query("CALCulate:MARKer:TABLe:STATe " +str(n[0]))
	def tracking_state(*n):
		if len(n) == 0:
			inst.query("CALCulate:MARKer:TRACking:STATe?")
		else:
			inst.query("CALCulate:MARKer:TRACking:STATe " +str(n[0]))

class ntdata:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def state(*n):
		if len(n) == 0:
			inst.query("CALCulate:NTData:STATe?")
		else:
			inst.query("CALCulate:NTData:STATe " +str(n[0]))
class calculate:
	def __init__(self):
		self.bandwidth=bandwidth()
		self.lline=lline()
		self.marker=marker()
		self.ntdata=ntdata()
	def (*n):
		if len(n) == 0:
			inst.query("CALibration:[ALL]?")
		else:
			inst.query("CALibration:[ALL] " +str(n[0]))

class auto:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def auto(*n):
		if len(n) == 0:
			inst.query("CALibration:AUTO?")
		else:
			inst.query("CALibration:AUTO " +str(n[0]))
class calibration:
	def __init__(self):
		self.auto=auto()
	def (*n):
		if len(n) == 0:
			inst.query("CONFigure??")
		else:
			inst.query("CONFigure? " +str(n[0]))
class CONFigure_2:
	def __init__(self):

########################################################		

class acpower:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def acpower(*n):
		if len(n) == 0:
			inst.query("CONFigure:ACPower?")
		else:
			inst.query("CONFigure:ACPower " +str(n[0]))

class chpower:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def chpower(*n):
		if len(n) == 0:
			inst.query("CONFigure:CHPower?")
		else:
			inst.query("CONFigure:CHPower " +str(n[0]))

class cnratio:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def cnratio(*n):
		if len(n) == 0:
			inst.query("CONFigure:CNRatio?")
		else:
			inst.query("CONFigure:CNRatio " +str(n[0]))

class ebwidth:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def ebwidth(*n):
		if len(n) == 0:
			inst.query("CONFigure:EBWidth?")
		else:
			inst.query("CONFigure:EBWidth " +str(n[0]))

class hdist:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def hdist(*n):
		if len(n) == 0:
			inst.query("CONFigure:HDISt?")
		else:
			inst.query("CONFigure:HDISt " +str(n[0]))

class obwidth:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def obwidth(*n):
		if len(n) == 0:
			inst.query("CONFigure:OBWidth?")
		else:
			inst.query("CONFigure:OBWidth " +str(n[0]))

class pf:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def pf(*n):
		if len(n) == 0:
			inst.query("CONFigure:PF?")
		else:
			inst.query("CONFigure:PF " +str(n[0]))

class sanalyzer:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def sanalyzer(*n):
		if len(n) == 0:
			inst.query("CONFigure:SANalyzer?")
		else:
			inst.query("CONFigure:SANalyzer " +str(n[0]))

class toi:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def toi(*n):
		if len(n) == 0:
			inst.query("CONFigure:TOI?")
		else:
			inst.query("CONFigure:TOI " +str(n[0]))

class tpower:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def tpower(*n):
		if len(n) == 0:
			inst.query("CONFigure:TPOWer?")
		else:
			inst.query("CONFigure:TPOWer " +str(n[0]))
class configure:
	def __init__(self):
		self.acpower=acpower()
		self.chpower=chpower()
		self.cnratio=cnratio()
		self.ebwidth=ebwidth()
		self.hdist=hdist()
		self.obwidth=obwidth()
		self.pf=pf()
		self.sanalyzer=sanalyzer()
		self.toi=toi()
		self.tpower=tpower()
	def (*n):
		if len(n) == 0:
			inst.query("COUPle?")
		else:
			inst.query("COUPle " +str(n[0]))
class COUPle:
	def __init__(self):

class afunction:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def position(*n):
		if len(n) == 0:
			inst.query("DISPlay:AFUnction:POSition?")
		else:
			inst.query("DISPlay:AFUnction:POSition " +str(n[0]))

class annotation:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def clock_state(*n):
		if len(n) == 0:
			inst.query("DISPlay:ANNotation:CLOCk:STATe?")
		else:
			inst.query("DISPlay:ANNotation:CLOCk:STATe " +str(n[0]))

class brightness:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def brightness(*n):
		if len(n) == 0:
			inst.query("DISPlay:BRIGhtness?")
		else:
			inst.write("DISPlay:BRIGhtness " +str(n[0]))

class enable:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def enable(*n):
		if len(n) == 0:
			inst.query("DISPlay:ENABle?")
		else:
			inst.query("DISPlay:ENABle " +str(n[0]))

class msgswitch:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def state(*n):
		if len(n) == 0:
			inst.query("DISPlay:MSGswitch:STATe?")
		else:
			inst.query("DISPlay:MSGswitch:STATe " +str(n[0]))

class ukey:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def state(*n):
		if len(n) == 0:
			inst.query("DISPlay:UKEY:STATe?")
		else:
			inst.query("DISPlay:UKEY:STATe " +str(n[0]))

class window:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def trace_graticule_grid(*n):
		if len(n) == 0:
			inst.query("DISPlay:WINdow:TRACe:GRATicule:GRID?")
		else:
			inst.query("DISPlay:WINdow:TRACe:GRATicule:GRID " +str(n[0]))
	def trace_x_scale_spacing(*n):
		if len(n) == 0:
			inst.query("DISPlay:WINdow:TRACe:X:SCALe:SPACing?")
		else:
			inst.query("DISPlay:WINdow:TRACe:X:SCALe:SPACing " +str(n[0]))
	def trace_y_dline(*n):
		if len(n) == 0:
			inst.query("DISPlay:WINdow:TRACe:Y:DLINe?")
		else:
			inst.query("DISPlay:WINdow:TRACe:Y:DLINe " +str(n[0]))
	def trace_y_dline_state(*n):
		if len(n) == 0:
			inst.query("DISPlay:WINdow:TRACe:Y:DLINe:STATe?")
		else:
			inst.query("DISPlay:WINdow:TRACe:Y:DLINe:STATe " +str(n[0]))
	def trace_y_scale_nrlevel(*n):
		if len(n) == 0:
			inst.query("DISPlay:WINdow:TRACe:Y:SCALe:NRLevel?")
		else:
			inst.query("DISPlay:WINdow:TRACe:Y:SCALe:NRLevel " +str(n[0]))
	def trace_y_scale_nrposition(*n):
		if len(n) == 0:
			inst.query("DISPlay:WINdow:TRACe:Y:SCALe:NRPosition?")
		else:
			inst.query("DISPlay:WINdow:TRACe:Y:SCALe:NRPosition " +str(n[0]))
	def trace_y_scale_pdivision(*n):
		if len(n) == 0:
			inst.query("DISPlay:WINdow:TRACe:Y:SCALe:PDIVision?")
		else:
			inst.query("DISPlay:WINdow:TRACe:Y:SCALe:PDIVision " +str(n[0]))
	def trace_y_scale_rlevel(*n):
		if len(n) == 0:
			inst.query("DISPlay:WINdow:TRACe:Y:SCALe:RLEVel?")
		else:
			inst.query("DISPlay:WINdow:TRACe:Y:SCALe:RLEVel " +str(n[0]))
	def trace_y_scale_rlevel_offset(*n):
		if len(n) == 0:
			inst.query("DISPlay:WINdow:TRACe:Y:SCALe:RLEVel:OFFSet?")
		else:
			inst.query("DISPlay:WINdow:TRACe:Y:SCALe:RLEVel:OFFSet " +str(n[0]))
	def trace_y_scale_spacing(*n):
		if len(n) == 0:
			inst.query("DISPlay:WINdow:TRACe:Y:SCALe:SPACing?")
		else:
			inst.query("DISPlay:WINdow:TRACe:Y:SCALe:SPACing " +str(n[0]))
class display:
	def __init__(self):
		self.afunction=afunction()
		self.annotation=annotation()
		self.brightness=brightness()
		self.enable=enable()
		self.msgswitch=msgswitch()
		self.ukey=ukey()
		self.window=window()

class acpower_2:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def acpower_2(*n):
		if len(n) == 0:
			inst.query("FETCh:ACPower??")
		else:
			inst.query("FETCh:ACPower? " +str(n[0]))

class acpower:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def lower_2(*n):
		if len(n) == 0:
			inst.query("FETCh:ACPower:LOWer??")
		else:
			inst.query("FETCh:ACPower:LOWer? " +str(n[0]))
	def main_2(*n):
		if len(n) == 0:
			inst.query("FETCh:ACPower:MAIN??")
		else:
			inst.query("FETCh:ACPower:MAIN? " +str(n[0]))
	def upper_2(*n):
		if len(n) == 0:
			inst.query("FETCh:ACPower:UPPer??")
		else:
			inst.query("FETCh:ACPower:UPPer? " +str(n[0]))

class chpower_2:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def chpower_2(*n):
		if len(n) == 0:
			inst.query("FETCh:CHPower??")
		else:
			inst.query("FETCh:CHPower? " +str(n[0]))

class chpower:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def chpower_2(*n):
		if len(n) == 0:
			inst.query("FETCh:CHPower:CHPower??")
		else:
			inst.query("FETCh:CHPower:CHPower? " +str(n[0]))
	def density_2(*n):
		if len(n) == 0:
			inst.query("FETCh:CHPower:DENSity??")
		else:
			inst.query("FETCh:CHPower:DENSity? " +str(n[0]))

class cnratio_2:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def cnratio_2(*n):
		if len(n) == 0:
			inst.query("FETCh:CNRatio??")
		else:
			inst.query("FETCh:CNRatio? " +str(n[0]))

class cnratio:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def carrier_2(*n):
		if len(n) == 0:
			inst.query("FETCh:CNRatio:CARRier??")
		else:
			inst.query("FETCh:CNRatio:CARRier? " +str(n[0]))
	def cnratio_2(*n):
		if len(n) == 0:
			inst.query("FETCh:CNRatio:CNRatio??")
		else:
			inst.query("FETCh:CNRatio:CNRatio? " +str(n[0]))
	def noise_2(*n):
		if len(n) == 0:
			inst.query("FETCh:CNRatio:NOISe??")
		else:
			inst.query("FETCh:CNRatio:NOISe? " +str(n[0]))

class ebwidth_2:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def ebwidth_2(*n):
		if len(n) == 0:
			inst.query("FETCh:EBWidth??")
		else:
			inst.query("FETCh:EBWidth? " +str(n[0]))

class harmonics:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def amplitude_all_2(*n):
		if len(n) == 0:
			inst.query("FETCh:HARMonics:AMPLitude:ALL??")
		else:
			inst.query("FETCh:HARMonics:AMPLitude:ALL? " +str(n[0]))
	def amplitude?<n>(*n):
		if len(n) == 0:
			inst.query("FETCh:HARMonics:AMPLitude?<n>?")
		else:
			inst.query("FETCh:HARMonics:AMPLitude?<n> " +str(n[0]))
	def distortion(*n):
		if len(n) == 0:
			inst.query("FETCh:HARMonics[:DISTortion]??")
		else:
			inst.query("FETCh:HARMonics[:DISTortion]? " +str(n[0]))
	def frequency_all_2(*n):
		if len(n) == 0:
			inst.query("FETCh:HARMonics:FREQuency:ALL??")
		else:
			inst.query("FETCh:HARMonics:FREQuency:ALL? " +str(n[0]))
	def frequency?<n>(*n):
		if len(n) == 0:
			inst.query("FETCh:HARMonics:FREQuency?<n>?")
		else:
			inst.query("FETCh:HARMonics:FREQuency?<n> " +str(n[0]))
	def fundamental_2(*n):
		if len(n) == 0:
			inst.query("FETCh:HARMonics:FUNDamental??")
		else:
			inst.query("FETCh:HARMonics:FUNDamental? " +str(n[0]))

class obwidth_2:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def obwidth_2(*n):
		if len(n) == 0:
			inst.query("FETCh:OBWidth??")
		else:
			inst.query("FETCh:OBWidth? " +str(n[0]))

class obwidth:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def obwidth_2(*n):
		if len(n) == 0:
			inst.query("FETCh:OBWidth:OBWidth??")
		else:
			inst.query("FETCh:OBWidth:OBWidth? " +str(n[0]))
	def obwidth_ferror_2(*n):
		if len(n) == 0:
			inst.query("FETCh:OBWidth:OBWidth:FERRor??")
		else:
			inst.query("FETCh:OBWidth:OBWidth:FERRor? " +str(n[0]))

class tointercept_2:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def tointercept_2(*n):
		if len(n) == 0:
			inst.query("FETCh:TOIntercept??")
		else:
			inst.query("FETCh:TOIntercept? " +str(n[0]))

class tointercept:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def ip3_2(*n):
		if len(n) == 0:
			inst.query("FETCh:TOIntercept:IP3??")
		else:
			inst.query("FETCh:TOIntercept:IP3? " +str(n[0]))

class tpower_2:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def tpower_2(*n):
		if len(n) == 0:
			inst.query("FETCh:TPOWer??")
		else:
			inst.query("FETCh:TPOWer? " +str(n[0]))
class fetch:
	def __init__(self):
		self.acpower?=acpower_2()
		self.acpower=acpower()
		self.chpower?=chpower_2()
		self.chpower=chpower()
		self.cnratio?=cnratio_2()
		self.cnratio=cnratio()
		self.ebwidth?=ebwidth_2()
		self.harmonics=harmonics()
		self.obwidth?=obwidth_2()
		self.obwidth=obwidth()
		self.tointercept?=tointercept_2()
		self.tointercept=tointercept()
		self.tpower?=tpower_2()

class border:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def border(*n):
		if len(n) == 0:
			inst.query("FORMat:BORDer?")
		else:
			inst.query("FORMat:BORDer " +str(n[0]))

class trace:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def data(*n):
		if len(n) == 0:
			inst.query("FORMat:TRACe][:DATA?")
		else:
			inst.query("FORMat:TRACe][:DATA " +str(n[0]))
class format:
	def __init__(self):
		self.border=border()
		self.trace=trace()

class abort:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def abort(*n):
		if len(n) == 0:
			inst.query("HCOPy:ABORt?")
		else:
			inst.query("HCOPy:ABORt " +str(n[0]))

class image:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def color_state(*n):
		if len(n) == 0:
			inst.query("HCOPy:IMAGe:COLor:STATe?")
		else:
			inst.query("HCOPy:IMAGe:COLor:STATe " +str(n[0]))
	def ftype(*n):
		if len(n) == 0:
			inst.query("HCOPy:IMAGe:FTYPe?")
		else:
			inst.query("HCOPy:IMAGe:FTYPe " +str(n[0]))
	def invert(*n):
		if len(n) == 0:
			inst.query("HCOPy:IMAGe:INVert?")
		else:
			inst.query("HCOPy:IMAGe:INVert " +str(n[0]))
	def ptime(*n):
		if len(n) == 0:
			inst.query("HCOPy:IMAGe:PTIMe?")
		else:
			inst.query("HCOPy:IMAGe:PTIMe " +str(n[0]))
	def quality(*n):
		if len(n) == 0:
			inst.query("HCOPy:IMAGe:QUALity?")
		else:
			inst.query("HCOPy:IMAGe:QUALity " +str(n[0]))

class immediate:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def immediate(*n):
		if len(n) == 0:
			inst.query("HCOPy:IMMediate?")
		else:
			inst.query("HCOPy:IMMediate " +str(n[0]))

class page:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def orientation(*n):
		if len(n) == 0:
			inst.query("HCOPy:PAGE:ORIentation?")
		else:
			inst.query("HCOPy:PAGE:ORIentation " +str(n[0]))
	def prints(*n):
		if len(n) == 0:
			inst.query("HCOPy:PAGE:PRINts?")
		else:
			inst.query("HCOPy:PAGE:PRINts " +str(n[0]))
	def size(*n):
		if len(n) == 0:
			inst.query("HCOPy:PAGE:SIZE?")
		else:
			inst.query("HCOPy:PAGE:SIZE " +str(n[0]))

class resume:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def resume(*n):
		if len(n) == 0:
			inst.query("HCOPy:RESume?")
		else:
			inst.query("HCOPy:RESume " +str(n[0]))
class hcopy:
	def __init__(self):
		self.abort=abort()
		self.image=image()
		self.immediate=immediate()
		self.page=page()
		self.resume=resume()

class continuous:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def continuous(*n):
		if len(n) == 0:
			inst.query("INITiate:CONTinuous?")
		else:
			inst.query("INITiate:CONTinuous " +str(n[0]))

class immediate:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def immediate(*n):
		if len(n) == 0:
			inst.query("INITiate:IMMediate?")
		else:
			inst.query("INITiate:IMMediate " +str(n[0]))

class pause:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def pause(*n):
		if len(n) == 0:
			inst.query("INITiate:PAUSe?")
		else:
			inst.query("INITiate:PAUSe " +str(n[0]))

class restart:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def restart(*n):
		if len(n) == 0:
			inst.query("INITiate:RESTart?")
		else:
			inst.query("INITiate:RESTart " +str(n[0]))

class resume:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def resume(*n):
		if len(n) == 0:
			inst.query("INITiate:RESume?")
		else:
			inst.query("INITiate:RESume " +str(n[0]))
class initiate:
	def __init__(self):
		self.continuous=continuous()
		self.immediate=immediate()
		self.pause=pause()
		self.restart=restart()
		self.resume=resume()

class impedance:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def impedance(*n):
		if len(n) == 0:
			inst.query("INPut:IMPedance?")
		else:
			inst.query("INPut:IMPedance " +str(n[0]))
class input:
	def __init__(self):
		self.impedance=impedance()

class delete:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def delete(*n):
		if len(n) == 0:
			inst.query("MMEMory:DELete?")
		else:
			inst.query("MMEMory:DELete " +str(n[0]))

class disk:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def information_2(*n):
		if len(n) == 0:
			inst.query("MMEMory:DISK:INFormation??")
		else:
			inst.query("MMEMory:DISK:INFormation? " +str(n[0]))

class load:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def correction(*n):
		if len(n) == 0:
			inst.query("MMEMory:LOAD:CORRection?")
		else:
			inst.query("MMEMory:LOAD:CORRection " +str(n[0]))
	def limit(*n):
		if len(n) == 0:
			inst.query("MMEMory:LOAD:LIMit?")
		else:
			inst.query("MMEMory:LOAD:LIMit " +str(n[0]))
	def mtable(*n):
		if len(n) == 0:
			inst.query("MMEMory:LOAD:MTABle?")
		else:
			inst.query("MMEMory:LOAD:MTABle " +str(n[0]))
	def setup(*n):
		if len(n) == 0:
			inst.query("MMEMory:LOAD:SETUp?")
		else:
			inst.query("MMEMory:LOAD:SETUp " +str(n[0]))
	def state(*n):
		if len(n) == 0:
			inst.query("MMEMory:LOAD:STATe?")
		else:
			inst.query("MMEMory:LOAD:STATe " +str(n[0]))
	def trace(*n):
		if len(n) == 0:
			inst.query("MMEMory:LOAD:TRACe?")
		else:
			inst.query("MMEMory:LOAD:TRACe " +str(n[0]))

class move:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def move(*n):
		if len(n) == 0:
			inst.query("MMEMory:MOVE?")
		else:
			inst.query("MMEMory:MOVE " +str(n[0]))

class store:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def correction(*n):
		if len(n) == 0:
			inst.query("MMEMory:STORe:CORRection?")
		else:
			inst.query("MMEMory:STORe:CORRection " +str(n[0]))
	def limit(*n):
		if len(n) == 0:
			inst.query("MMEMory:STORe:LIMit?")
		else:
			inst.query("MMEMory:STORe:LIMit " +str(n[0]))
	def mtable(*n):
		if len(n) == 0:
			inst.query("MMEMory:STORe:MTABle?")
		else:
			inst.query("MMEMory:STORe:MTABle " +str(n[0]))
	def ptable(*n):
		if len(n) == 0:
			inst.query("MMEMory:STORe:PTABle?")
		else:
			inst.query("MMEMory:STORe:PTABle " +str(n[0]))
	def results(*n):
		if len(n) == 0:
			inst.query("MMEMory:STORe:RESults?")
		else:
			inst.query("MMEMory:STORe:RESults " +str(n[0]))
	def screen(*n):
		if len(n) == 0:
			inst.query("MMEMory:STORe:SCReen?")
		else:
			inst.query("MMEMory:STORe:SCReen " +str(n[0]))
	def setup(*n):
		if len(n) == 0:
			inst.query("MMEMory:STORe:SETUp?")
		else:
			inst.query("MMEMory:STORe:SETUp " +str(n[0]))
	def state(*n):
		if len(n) == 0:
			inst.query("MMEMory:STORe:STATe?")
		else:
			inst.query("MMEMory:STORe:STATe " +str(n[0]))
	def trace(*n):
		if len(n) == 0:
			inst.query("MMEMory:STORe:TRACe?")
		else:
			inst.query("MMEMory:STORe:TRACe " +str(n[0]))
class mmemory:
	def __init__(self):
		self.delete=delete()
		self.disk=disk()
		self.load=load()
		self.move=move()
		self.store=store()

class state:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def state(*n):
		if len(n) == 0:
			inst.query("OUTPut:STATe?")
		else:
			inst.query("OUTPut:STATe " +str(n[0]))
class output:
	def __init__(self):
		self.state=state()

class acpower_2:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def acpower_2(*n):
		if len(n) == 0:
			inst.query("READ:ACPower??")
		else:
			inst.query("READ:ACPower? " +str(n[0]))

class acpower:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def lower_2(*n):
		if len(n) == 0:
			inst.query("READ:ACPower:LOWer??")
		else:
			inst.query("READ:ACPower:LOWer? " +str(n[0]))
	def main_2(*n):
		if len(n) == 0:
			inst.query("READ:ACPower:MAIN??")
		else:
			inst.query("READ:ACPower:MAIN? " +str(n[0]))
	def upper_2(*n):
		if len(n) == 0:
			inst.query("READ:ACPower:UPPer??")
		else:
			inst.query("READ:ACPower:UPPer? " +str(n[0]))

class chpower_2:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def chpower_2(*n):
		if len(n) == 0:
			inst.query("READ:CHPower??")
		else:
			inst.query("READ:CHPower? " +str(n[0]))

class chpower:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def chpower_2(*n):
		if len(n) == 0:
			inst.query("READ:CHPower:CHPower??")
		else:
			inst.query("READ:CHPower:CHPower? " +str(n[0]))
	def density_2(*n):
		if len(n) == 0:
			inst.query("READ:CHPower:DENSity??")
		else:
			inst.query("READ:CHPower:DENSity? " +str(n[0]))

class cnratio_2:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def cnratio_2(*n):
		if len(n) == 0:
			inst.query("READ:CNRatio??")
		else:
			inst.query("READ:CNRatio? " +str(n[0]))

class cnratio:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def carrier_2(*n):
		if len(n) == 0:
			inst.query("READ:CNRatio:CARRier??")
		else:
			inst.query("READ:CNRatio:CARRier? " +str(n[0]))
	def cnratio_2(*n):
		if len(n) == 0:
			inst.query("READ:CNRatio:CNRatio??")
		else:
			inst.query("READ:CNRatio:CNRatio? " +str(n[0]))
	def noise_2(*n):
		if len(n) == 0:
			inst.query("READ:CNRatio:NOISe??")
		else:
			inst.query("READ:CNRatio:NOISe? " +str(n[0]))

class ebwidth_2:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def ebwidth_2(*n):
		if len(n) == 0:
			inst.query("READ:EBWidth??")
		else:
			inst.query("READ:EBWidth? " +str(n[0]))

class harmonics:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def amplitude_all_2(*n):
		if len(n) == 0:
			inst.query("READ:HARMonics:AMPLitude:ALL??")
		else:
			inst.query("READ:HARMonics:AMPLitude:ALL? " +str(n[0]))
	def amplitude?<n>(*n):
		if len(n) == 0:
			inst.query("READ:HARMonics:AMPLitude?<n>?")
		else:
			inst.query("READ:HARMonics:AMPLitude?<n> " +str(n[0]))
	def distortion(*n):
		if len(n) == 0:
			inst.query("READ:HARMonics[:DISTortion]??")
		else:
			inst.query("READ:HARMonics[:DISTortion]? " +str(n[0]))
	def frequency_all_2(*n):
		if len(n) == 0:
			inst.query("READ:HARMonics:FREQuency:ALL??")
		else:
			inst.query("READ:HARMonics:FREQuency:ALL? " +str(n[0]))
	def frequency?<n>(*n):
		if len(n) == 0:
			inst.query("READ:HARMonics:FREQuency?<n>?")
		else:
			inst.query("READ:HARMonics:FREQuency?<n> " +str(n[0]))
	def fundamental_2(*n):
		if len(n) == 0:
			inst.query("READ:HARMonics:FUNDamental??")
		else:
			inst.query("READ:HARMonics:FUNDamental? " +str(n[0]))

class obwidth_2:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def obwidth_2(*n):
		if len(n) == 0:
			inst.query("READ:OBWidth??")
		else:
			inst.query("READ:OBWidth? " +str(n[0]))

class obwidth:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def obwidth_2(*n):
		if len(n) == 0:
			inst.query("READ:OBWidth:OBWidth??")
		else:
			inst.query("READ:OBWidth:OBWidth? " +str(n[0]))
	def obwidth_ferror_2(*n):
		if len(n) == 0:
			inst.query("READ:OBWidth:OBWidth:FERRor??")
		else:
			inst.query("READ:OBWidth:OBWidth:FERRor? " +str(n[0]))

class tointercept_2:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def tointercept_2(*n):
		if len(n) == 0:
			inst.query("READ:TOIntercept??")
		else:
			inst.query("READ:TOIntercept? " +str(n[0]))

class tointercept:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def ip3_2(*n):
		if len(n) == 0:
			inst.query("READ:TOIntercept:IP3??")
		else:
			inst.query("READ:TOIntercept:IP3? " +str(n[0]))

class tpower_2:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def tpower_2(*n):
		if len(n) == 0:
			inst.query("READ:TPOWer??")
		else:
			inst.query("READ:TPOWer? " +str(n[0]))
class read:
	def __init__(self):
		self.acpower?=acpower_2()
		self.acpower=acpower()
		self.chpower?=chpower_2()
		self.chpower=chpower()
		self.cnratio?=cnratio_2()
		self.cnratio=cnratio()
		self.ebwidth?=ebwidth_2()
		self.harmonics=harmonics()
		self.obwidth?=obwidth_2()
		self.obwidth=obwidth()
		self.tointercept?=tointercept_2()
		self.tointercept=tointercept()
		self.tpower?=tpower_2()

class acpower:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	
	#revisada a mano
	def average_count(*n):
		if len(n) == 0:
			inst.query("SENSe:ACPower:AVERage:COUNt?")
		else:
			inst.query("SENSe:ACPower:AVERage:COUNt " +str(n[0]))

	#revisada a mano
	def average_state(*n):
		if len(n) == 0:
			inst.query("SENSe:ACPower:AVERage:STATe?")
		else:
			inst.query("SENSe:ACPower:AVERage:STATe " +str(n[0]))

	def average_tcontrol(*n):
		if len(n) == 0:
			inst.query("SENSe:ACPower:AVERage:TCONtrol?")
		else:
			inst.query("SENSe:ACPower:AVERage:TCONtrol " +str(n[0]))

	#revisada a mano		
	def bandwidth_achannel(*n):
		if len(n) == 0:
			inst.query("SENSe:ACPower:BANDwidth:ACHannel?")
		else:
			inst.query("SENSe:ACPower:BANDwidth:ACHannel " +str(n[0]))

	#revisada a mano		
	def bandwidth_integration(*n):
		if len(n) == 0:
			inst.query("SENSe:ACPower:BANDwidth:INTegration?")
		else:
			inst.query("SENSe:ACPower:BANDwidth:INTegration " +str(n[0]))
	#revisada a mano		
	def cspacing(*n):
		if len(n) == 0:
			inst.query("SENSe:ACPower:CSPacing?")
		else:
			inst.query("SENSe:ACPower:CSPacing " +str(n[0]))

class bandwidth:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def emifilter_state(*n):
		if len(n) == 0:
			inst.query("SENSe:BANDwidth:EMIFilter:STATe?")
		else:
			inst.query("SENSe:BANDwidth:EMIFilter:STATe " +str(n[0]))

	#revisada a mano		
	def resolution(*n):
		if len(n) == 0:
			inst.query("SENSe:BANDwidth:RESolution?")
		else:
			inst.query("SENSe:BANDwidth:RESolution " +str(n[0]))

	#revisada a mano
	def resolution_auto(*n):
		if len(n) == 0:
			inst.query("SENSe:BANDwidth:RESolution:AUTO?")
		else:
			inst.query("SENSe:BANDwidth:RESolution:AUTO " +str(n[0]))

	#revisada a mano
	def video(*n):
		if len(n) == 0:
			inst.query("SENSe:BANDwidth:VIDeo?")
		else:
			inst.query("SENSe:BANDwidth:VIDeo " +str(n[0]))

	#revisada a mano
	def video_auto(*n):
		if len(n) == 0:
			inst.query("SENSe:BANDwidth:VIDeo:AUTO?")
		else:
			inst.query("SENSe:BANDwidth:VIDeo:AUTO " +str(n[0]))

	#revisada a mano		
	def video_ratio(*n):
		if len(n) == 0:
			inst.query("SENSe:BANDwidth:VIDeo:RATio?")
		else:
			inst.query("SENSe:BANDwidth:VIDeo:RATio " +str(n[0]))

class chpower:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"

	#revisada a mano	
	def average_count(*n):
		if len(n) == 0:
			inst.query("SENSe:CHPower:AVERage:COUNt?")
		else:
			inst.query("SENSe:CHPower:AVERage:COUNt " +str(n[0]))

	def average_state(*n):
		if len(n) == 0:
			inst.query("SENSe:CHPower:AVERage:STATe?")
		else:
			inst.query("SENSe:CHPower:AVERage:STATe " +str(n[0]))
	def average_tcontrol(*n):
		if len(n) == 0:
			inst.query("SENSe:CHPower:AVERage:TCONtrol?")
		else:
			inst.query("SENSe:CHPower:AVERage:TCONtrol " +str(n[0]))
	def bandwidth_integration(*n):
		if len(n) == 0:
			inst.query("SENSe:CHPower:BANDwidth:INTegration?")
		else:
			inst.query("SENSe:CHPower:BANDwidth:INTegration " +str(n[0]))

	#revisada a mano
	def frequency_span(*n):
		if len(n) == 0:
			inst.query("SENSe:CHPower:FREQuency:SPAN?")
		else:
			inst.query("SENSe:CHPower:FREQuency:SPAN " +str(n[0]))

# carrier to noise
class cnratio:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def average_count(*n):
		if len(n) == 0:
			inst.query("SENSe:CNRatio:AVERage:COUNt?")
		else:
			inst.query("SENSe:CNRatio:AVERage:COUNt " +str(n[0]))
	def average_state(*n):
		if len(n) == 0:
			inst.query("SENSe:CNRatio:AVERage:STATe?")
		else:
			inst.query("SENSe:CNRatio:AVERage:STATe " +str(n[0]))
	def average_tcontrol(*n):
		if len(n) == 0:
			inst.query("SENSe:CNRatio:AVERage:TCONtrol?")
		else:
			inst.query("SENSe:CNRatio:AVERage:TCONtrol " +str(n[0]))
	def bandwidth_integration(*n):
		if len(n) == 0:
			inst.query("SENSe:CNRatio:BANDwidth:INTegration?")
		else:
			inst.query("SENSe:CNRatio:BANDwidth:INTegration " +str(n[0]))
	def bandwidth_noise(*n):
		if len(n) == 0:
			inst.query("SENSe:CNRatio:BANDwidth:NOISe?")
		else:
			inst.query("SENSe:CNRatio:BANDwidth:NOISe " +str(n[0]))
	def offset(*n):
		if len(n) == 0:
			inst.query("SENSe:CNRatio:OFFSet?")
		else:
			inst.query("SENSe:CNRatio:OFFSet " +str(n[0]))

class correction:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"

	#revisada a mano
	def cset_all_delete():
			inst.query("SENSe:CORRection:CSET:ALL:DELete")

	def cset_all_state(*n):
		if len(n) == 0:
			inst.query("SENSe:CORRection:CSET:ALL:STATe?")
		else:
			inst.query("SENSe:CORRection:CSET:ALL:STATe " +str(n[0]))
	def cset<n>_data(*n):
		if len(n) == 0:
			inst.query("SENSe:CORRection:CSET<n>:DATA?")
		else:
			inst.query("SENSe:CORRection:CSET<n>:DATA " +str(n[0]))
	def cset<n>_data_merge(*n):
		if len(n) == 0:
			inst.query("SENSe:CORRection:CSET<n>:DATA:MERGe?")
		else:
			inst.query("SENSe:CORRection:CSET<n>:DATA:MERGe " +str(n[0]))
	def cset<n>_delete(*n):
		if len(n) == 0:
			inst.query("SENSe:CORRection:CSET<n>:DELete?")
		else:
			inst.query("SENSe:CORRection:CSET<n>:DELete " +str(n[0]))
	def cset<n>_state(*n):
		if len(n) == 0:
			inst.query("SENSe:CORRection:CSET<n>:STATe?")
		else:
			inst.query("SENSe:CORRection:CSET<n>:STATe " +str(n[0]))
	def cset<n>_x_spacing(*n):
		if len(n) == 0:
			inst.query("SENSe:CORRection:CSET<n>:X:SPACing?")
		else:
			inst.query("SENSe:CORRection:CSET<n>:X:SPACing " +str(n[0]))
	def cset_table_state(*n):
		if len(n) == 0:
			inst.query("SENSe:CORRection:CSET:TABLe:STATe?")
		else:
			inst.query("SENSe:CORRection:CSET:TABLe:STATe " +str(n[0]))

class demod:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"

	#revisada a mano
	def demod(*n):
		if len(n) == 0:
			inst.query("SENSe:DEMod?")
		else:
			inst.query("SENSe:DEMod " +str(n[0]))

	#revisada a mano
	def gain_auto(*n):
		if len(n) == 0:
			inst.query("SENSe:DEMod:GAIN:AUTO?")
		else:
			inst.query("SENSe:DEMod:GAIN:AUTO " +str(n[0]))

	#revisada a mano
	def gain_increment(*n):
		if len(n) == 0:
			inst.query("SENSe:DEMod:GAIN:INCRement?")
		else:
			inst.query("SENSe:DEMod:GAIN:INCRement " +str(n[0]))


	def state(*n):
		if len(n) == 0:
			inst.query("SENSe:DEMod:STATe?")
		else:
			inst.query("SENSe:DEMod:STATe " +str(n[0]))
	def time(*n):
		if len(n) == 0:
			inst.query("SENSe:DEMod:TIME?")
		else:
			inst.query("SENSe:DEMod:TIME " +str(n[0]))

class detector:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def function(*n):
		if len(n) == 0:
			inst.query("SENSe:DETector:FUNCtion?")
		else:
			inst.query("SENSe:DETector:FUNCtion " +str(n[0]))

class ebwidth:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def average_count(*n):
		if len(n) == 0:
			inst.query("SENSe:EBWidth:AVERage:COUNt?")
		else:
			inst.query("SENSe:EBWidth:AVERage:COUNt " +str(n[0]))
	def average_state(*n):
		if len(n) == 0:
			inst.query("SENSe:EBWidth:AVERage:STATe?")
		else:
			inst.query("SENSe:EBWidth:AVERage:STATe " +str(n[0]))
	def average_tcontrol(*n):
		if len(n) == 0:
			inst.query("SENSe:EBWidth:AVERage:TCONtrol?")
		else:
			inst.query("SENSe:EBWidth:AVERage:TCONtrol " +str(n[0]))
	def frequency_span(*n):
		if len(n) == 0:
			inst.query("SENSe:EBWidth:FREQuency:SPAN?")
		else:
			inst.query("SENSe:EBWidth:FREQuency:SPAN " +str(n[0]))
	def maxhold_state(*n):
		if len(n) == 0:
			inst.query("SENSe:EBWidth:MAXHold:STATe?")
		else:
			inst.query("SENSe:EBWidth:MAXHold:STATe " +str(n[0]))
	def xdb(*n):
		if len(n) == 0:
			inst.query("SENSe:EBWidth:XDB?")
		else:
			inst.query("SENSe:EBWidth:XDB " +str(n[0]))

class extref:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def state(*n):
		if len(n) == 0:
			inst.query("SENSe:EXTRef[:STATe]??")
		else:
			inst.query("SENSe:EXTRef[:STATe]? " +str(n[0]))

class frequency:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def center(*n):
		if len(n) == 0:
			inst.query("SENSe:FREQuency:CENTer?")
		else:
			inst.query("SENSe:FREQuency:CENTer " +str(n[0]))
	def center_down(*n):
		if len(n) == 0:
			inst.query("SENSe:FREQuency:CENTer:DOWN?")
		else:
			inst.query("SENSe:FREQuency:CENTer:DOWN " +str(n[0]))
	def center_set_step(*n):
		if len(n) == 0:
			inst.query("SENSe:FREQuency:CENTer:SET:STEP?")
		else:
			inst.query("SENSe:FREQuency:CENTer:SET:STEP " +str(n[0]))
	def center_step_auto(*n):
		if len(n) == 0:
			inst.query("SENSe:FREQuency:CENTer:STEP:AUTO?")
		else:
			inst.query("SENSe:FREQuency:CENTer:STEP:AUTO " +str(n[0]))
	def center_step_increment(*n):
		if len(n) == 0:
			inst.query("SENSe:FREQuency:CENTer:STEP:INCRement?")
		else:
			inst.query("SENSe:FREQuency:CENTer:STEP:INCRement " +str(n[0]))
	def center_up(*n):
		if len(n) == 0:
			inst.query("SENSe:FREQuency:CENTer:UP?")
		else:
			inst.query("SENSe:FREQuency:CENTer:UP " +str(n[0]))
	def offset(*n):
		if len(n) == 0:
			inst.query("SENSe:FREQuency:OFFSet?")
		else:
			inst.query("SENSe:FREQuency:OFFSet " +str(n[0]))
	def span(*n):
		if len(n) == 0:
			inst.query("SENSe:FREQuency:SPAN?")
		else:
			inst.query("SENSe:FREQuency:SPAN " +str(n[0]))
	def span_full(*n):
		if len(n) == 0:
			inst.query("SENSe:FREQuency:SPAN:FULL?")
		else:
			inst.query("SENSe:FREQuency:SPAN:FULL " +str(n[0]))
	def span_previous(*n):
		if len(n) == 0:
			inst.query("SENSe:FREQuency:SPAN:PREVious?")
		else:
			inst.query("SENSe:FREQuency:SPAN:PREVious " +str(n[0]))
	def span_zin(*n):
		if len(n) == 0:
			inst.query("SENSe:FREQuency:SPAN:ZIN?")
		else:
			inst.query("SENSe:FREQuency:SPAN:ZIN " +str(n[0]))
	def span_zout(*n):
		if len(n) == 0:
			inst.query("SENSe:FREQuency:SPAN:ZOUT?")
		else:
			inst.query("SENSe:FREQuency:SPAN:ZOUT " +str(n[0]))
	def start(*n):
		if len(n) == 0:
			inst.query("SENSe:FREQuency:STARt?")
		else:
			inst.query("SENSe:FREQuency:STARt " +str(n[0]))
	def stop(*n):
		if len(n) == 0:
			inst.query("SENSe:FREQuency:STOP?")
		else:
			inst.query("SENSe:FREQuency:STOP " +str(n[0]))

class hdist:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def average_count(*n):
		if len(n) == 0:
			inst.query("SENSe:HDISt:AVERage:COUNt?")
		else:
			inst.query("SENSe:HDISt:AVERage:COUNt " +str(n[0]))
	def average_state(*n):
		if len(n) == 0:
			inst.query("SENSe:HDISt:AVERage:STATe?")
		else:
			inst.query("SENSe:HDISt:AVERage:STATe " +str(n[0]))
	def average_tcontrol(*n):
		if len(n) == 0:
			inst.query("SENSe:HDISt:AVERage:TCONtrol?")
		else:
			inst.query("SENSe:HDISt:AVERage:TCONtrol " +str(n[0]))
	def numbers(*n):
		if len(n) == 0:
			inst.query("SENSe:HDISt:NUMBers?")
		else:
			inst.query("SENSe:HDISt:NUMBers " +str(n[0]))
	def time(*n):
		if len(n) == 0:
			inst.query("SENSe:HDISt:TIME?")
		else:
			inst.query("SENSe:HDISt:TIME " +str(n[0]))
	def time_auto_state(*n):
		if len(n) == 0:
			inst.query("SENSe:HDISt:TIME:AUTO:STATe?")
		else:
			inst.query("SENSe:HDISt:TIME:AUTO:STATe " +str(n[0]))

class obwidth:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def average_count(*n):
		if len(n) == 0:
			inst.query("SENSe:OBWidth:AVERage:COUNt?")
		else:
			inst.query("SENSe:OBWidth:AVERage:COUNt " +str(n[0]))
	def average_state(*n):
		if len(n) == 0:
			inst.query("SENSe:OBWidth:AVERage:STATe?")
		else:
			inst.query("SENSe:OBWidth:AVERage:STATe " +str(n[0]))
	def average_tcontrol(*n):
		if len(n) == 0:
			inst.query("SENSe:OBWidth:AVERage:TCONtrol?")
		else:
			inst.query("SENSe:OBWidth:AVERage:TCONtrol " +str(n[0]))
	def frequency_span(*n):
		if len(n) == 0:
			inst.query("SENSe:OBWidth:FREQuency:SPAN?")
		else:
			inst.query("SENSe:OBWidth:FREQuency:SPAN " +str(n[0]))
	def maxhold_state(*n):
		if len(n) == 0:
			inst.query("SENSe:OBWidth:MAXHold:STATe?")
		else:
			inst.query("SENSe:OBWidth:MAXHold:STATe " +str(n[0]))
	def percent(*n):
		if len(n) == 0:
			inst.query("SENSe:OBWidth:PERCent?")
		else:
			inst.query("SENSe:OBWidth:PERCent " +str(n[0]))

class power:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def arange(*n):
		if len(n) == 0:
			inst.query("SENSe:POWer:ARANge?")
		else:
			inst.query("SENSe:POWer:ARANge " +str(n[0]))
	def ascale(*n):
		if len(n) == 0:
			inst.query("SENSe:POWer:ASCale?")
		else:
			inst.query("SENSe:POWer:ASCale " +str(n[0]))
	def atune(*n):
		if len(n) == 0:
			inst.query("SENSe:POWer:ATUNe?")
		else:
			inst.query("SENSe:POWer:ATUNe " +str(n[0]))
	def rf_attenuation(*n):
		if len(n) == 0:
			inst.query("SENSe:POWer:RF:ATTenuation?")
		else:
			inst.query("SENSe:POWer:RF:ATTenuation " +str(n[0]))
	def rf_attenuation_auto(*n):
		if len(n) == 0:
			inst.query("SENSe:POWer:RF:ATTenuation:AUTO?")
		else:
			inst.query("SENSe:POWer:RF:ATTenuation:AUTO " +str(n[0]))
	def rf_gain_state(*n):
		if len(n) == 0:
			inst.query("SENSe:POWer:RF:GAIN:STATe?")
		else:
			inst.query("SENSe:POWer:RF:GAIN:STATe " +str(n[0]))
	def rf_mixer_range_upper(*n):
		if len(n) == 0:
			inst.query("SENSe:POWer:RF:MIXer:RANGe:UPPer?")
		else:
			inst.query("SENSe:POWer:RF:MIXer:RANGe:UPPer " +str(n[0]))

class sigcapture:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def state(*n):
		if len(n) == 0:
			inst.query("SENSe:SIGCapture:STATe?")
		else:
			inst.query("SENSe:SIGCapture:STATe " +str(n[0]))
	def sigc_state(*n):
		if len(n) == 0:
			inst.query("SENSe:SIGCapture:SIGC:STATe?")
		else:
			inst.query("SENSe:SIGCapture:SIGC:STATe " +str(n[0]))
	def maxhold_state(*n):
		if len(n) == 0:
			inst.query("SENSe:SIGCapture:MAXHold:STATe?")
		else:
			inst.query("SENSe:SIGCapture:MAXHold:STATe " +str(n[0]))
	def reset(*n):
		if len(n) == 0:
			inst.query("SENSe:SIGCapture:RESet?")
		else:
			inst.query("SENSe:SIGCapture:RESet " +str(n[0]))
	def 2fsk_state(*n):
		if len(n) == 0:
			inst.query("SENSe:SIGCapture:2FSK:STATe?")
		else:
			inst.query("SENSe:SIGCapture:2FSK:STATe " +str(n[0]))
	def 2fsk_reset(*n):
		if len(n) == 0:
			inst.query("SENSe:SIGCapture:2FSK:RESet?")
		else:
			inst.query("SENSe:SIGCapture:2FSK:RESet " +str(n[0]))
	def 2fsk_maxhold_state(*n):
		if len(n) == 0:
			inst.query("SENSe:SIGCapture:2FSK:MAXHold:STATe?")
		else:
			inst.query("SENSe:SIGCapture:2FSK:MAXHold:STATe " +str(n[0]))
	def 2fsk_pfswitch(*n):
		if len(n) == 0:
			inst.query("SENSe:SIGCapture:2FSK:PFSWitch?")
		else:
			inst.query("SENSe:SIGCapture:2FSK:PFSWitch " +str(n[0]))
	def 2fsk_signal(*n):
		if len(n) == 0:
			inst.query("SENSe:SIGCapture:2FSK:SIGNal?")
		else:
			inst.query("SENSe:SIGCapture:2FSK:SIGNal " +str(n[0]))
	def 2fsk_ampup(*n):
		if len(n) == 0:
			inst.query("SENSe:SIGCapture:2FSK:AMPUp?")
		else:
			inst.query("SENSe:SIGCapture:2FSK:AMPUp " +str(n[0]))
	def 2fsk_ampdown(*n):
		if len(n) == 0:
			inst.query("SENSe:SIGCapture:2FSK:AMPDown?")
		else:
			inst.query("SENSe:SIGCapture:2FSK:AMPDown " +str(n[0]))
	def 2fsk_mark1_freq(*n):
		if len(n) == 0:
			inst.query("SENSe:SIGCapture:2FSK:MARK1:FREQ?")
		else:
			inst.query("SENSe:SIGCapture:2FSK:MARK1:FREQ " +str(n[0]))
	def 2fsk_mark1_switch_state(*n):
		if len(n) == 0:
			inst.query("SENSe:SIGCapture:2FSK:MARK1:Switch:STATe?")
		else:
			inst.query("SENSe:SIGCapture:2FSK:MARK1:Switch:STATe " +str(n[0]))
	def 2fsk_mark2_freq(*n):
		if len(n) == 0:
			inst.query("SENSe:SIGCapture:2FSK:MARK2:FREQ?")
		else:
			inst.query("SENSe:SIGCapture:2FSK:MARK2:FREQ " +str(n[0]))
	def 2fsk_mark2_switch_state(*n):
		if len(n) == 0:
			inst.query("SENSe:SIGCapture:2FSK:MARK2:Switch:STATe?")
		else:
			inst.query("SENSe:SIGCapture:2FSK:MARK2:Switch:STATe " +str(n[0]))

class sweep:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def count(*n):
		if len(n) == 0:
			inst.query("SENSe:SWEep:COUNt?")
		else:
			inst.query("SENSe:SWEep:COUNt " +str(n[0]))
	def count_current_2(*n):
		if len(n) == 0:
			inst.query("SENSe:SWEep:COUNt:CURRent??")
		else:
			inst.query("SENSe:SWEep:COUNt:CURRent? " +str(n[0]))
	def points(*n):
		if len(n) == 0:
			inst.query("SENSe:SWEep:POINts?")
		else:
			inst.query("SENSe:SWEep:POINts " +str(n[0]))
	def time(*n):
		if len(n) == 0:
			inst.query("SENSe:SWEep:TIME?")
		else:
			inst.query("SENSe:SWEep:TIME " +str(n[0]))
	def time_auto(*n):
		if len(n) == 0:
			inst.query("SENSe:SWEep:TIME:AUTO?")
		else:
			inst.query("SENSe:SWEep:TIME:AUTO " +str(n[0]))
	def time_auto_rules(*n):
		if len(n) == 0:
			inst.query("SENSe:SWEep:TIME:AUTO:RULes?")
		else:
			inst.query("SENSe:SWEep:TIME:AUTO:RULes " +str(n[0]))

class toi:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def average_count(*n):
		if len(n) == 0:
			inst.query("SENSe:TOI:AVERage:COUNt?")
		else:
			inst.query("SENSe:TOI:AVERage:COUNt " +str(n[0]))
	def average_state(*n):
		if len(n) == 0:
			inst.query("SENSe:TOI:AVERage:STATe?")
		else:
			inst.query("SENSe:TOI:AVERage:STATe " +str(n[0]))
	def average_tcontrol(*n):
		if len(n) == 0:
			inst.query("SENSe:TOI:AVERage:TCONtrol?")
		else:
			inst.query("SENSe:TOI:AVERage:TCONtrol " +str(n[0]))
	def frequency_span(*n):
		if len(n) == 0:
			inst.query("SENSe:TOI:FREQuency:SPAN?")
		else:
			inst.query("SENSe:TOI:FREQuency:SPAN " +str(n[0]))

class tpower:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def average_count(*n):
		if len(n) == 0:
			inst.query("SENSe:TPOWer:AVERage:COUNt?")
		else:
			inst.query("SENSe:TPOWer:AVERage:COUNt " +str(n[0]))
	def average_state(*n):
		if len(n) == 0:
			inst.query("SENSe:TPOWer:AVERage:STATe?")
		else:
			inst.query("SENSe:TPOWer:AVERage:STATe " +str(n[0]))
	def average_tcontrol(*n):
		if len(n) == 0:
			inst.query("SENSe:TPOWer:AVERage:TCONtrol?")
		else:
			inst.query("SENSe:TPOWer:AVERage:TCONtrol " +str(n[0]))
	def llimit(*n):
		if len(n) == 0:
			inst.query("SENSe:TPOWer:LLIMit?")
		else:
			inst.query("SENSe:TPOWer:LLIMit " +str(n[0]))
	def mode(*n):
		if len(n) == 0:
			inst.query("SENSe:TPOWer:MODE?")
		else:
			inst.query("SENSe:TPOWer:MODE " +str(n[0]))
	def rlimit(*n):
		if len(n) == 0:
			inst.query("SENSe:TPOWer:RLIMit?")
		else:
			inst.query("SENSe:TPOWer:RLIMit " +str(n[0]))

class vswr:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def freflect(*n):
		if len(n) == 0:
			inst.query("SENSe:VSWR:FREFlect?")
		else:
			inst.query("SENSe:VSWR:FREFlect " +str(n[0]))
	def nreflect(*n):
		if len(n) == 0:
			inst.query("SENSe:VSWR:NREFlect?")
		else:
			inst.query("SENSe:VSWR:NREFlect " +str(n[0]))
	def reset(*n):
		if len(n) == 0:
			inst.query("SENSe:VSWR:RESet?")
		else:
			inst.query("SENSe:VSWR:RESet " +str(n[0]))
	def state(*n):
		if len(n) == 0:
			inst.query("SENSe:VSWR:STATe?")
		else:
			inst.query("SENSe:VSWR:STATe " +str(n[0]))
class sense:
	def __init__(self):
		self.acpower=acpower()
		self.bandwidth=bandwidth()
		self.chpower=chpower()
		self.cnratio=cnratio()
		self.correction=correction()
		self.demod=demod()
		self.detector=detector()
		self.ebwidth=ebwidth()
		self.extref=extref()
		self.frequency=frequency()
		self.hdist=hdist()
		self.obwidth=obwidth()
		self.power=power()
		self.sigcapture=sigcapture()
		self.sweep=sweep()
		self.toi=toi()
		self.tpower=tpower()
		self.vswr=vswr()

class correction:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def offset(*n):
		if len(n) == 0:
			inst.query("SOURce:CORRection:OFFSet?")
		else:
			inst.query("SOURce:CORRection:OFFSet " +str(n[0]))

class power:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def level_immediate_amplitude(*n):
		if len(n) == 0:
			inst.query("SOURce:POWer:LEVel:IMMediate:AMPLitude?")
		else:
			inst.query("SOURce:POWer:LEVel:IMMediate:AMPLitude " +str(n[0]))
	def mode(*n):
		if len(n) == 0:
			inst.query("SOURce:POWer:MODE?")
		else:
			inst.query("SOURce:POWer:MODE " +str(n[0]))
	def span(*n):
		if len(n) == 0:
			inst.query("SOURce:POWer:SPAN?")
		else:
			inst.query("SOURce:POWer:SPAN " +str(n[0]))
	def start(*n):
		if len(n) == 0:
			inst.query("SOURce:POWer:STARt?")
		else:
			inst.query("SOURce:POWer:STARt " +str(n[0]))
	def sweep(*n):
		if len(n) == 0:
			inst.query("SOURce:POWer:SWEep?")
		else:
			inst.query("SOURce:POWer:SWEep " +str(n[0]))

class trace:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def ref_state(*n):
		if len(n) == 0:
			inst.query("SOURce:TRACe:REF:STATe?")
		else:
			inst.query("SOURce:TRACe:REF:STATe " +str(n[0]))
	def storref(*n):
		if len(n) == 0:
			inst.query("SOURce:TRACe:STORref?")
		else:
			inst.query("SOURce:TRACe:STORref " +str(n[0]))
class source:
	def __init__(self):
		self.correction=correction()
		self.power=power()
		self.trace=trace()

class operation:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def condition_2(*n):
		if len(n) == 0:
			inst.query("STATus:OPERation:CONDition??")
		else:
			inst.query("STATus:OPERation:CONDition? " +str(n[0]))
	def enable(*n):
		if len(n) == 0:
			inst.query("STATus:OPERation:ENABle?")
		else:
			inst.query("STATus:OPERation:ENABle " +str(n[0]))
	def event(*n):
		if len(n) == 0:
			inst.query("STATus:OPERation[:EVENt]??")
		else:
			inst.query("STATus:OPERation[:EVENt]? " +str(n[0]))

class preset:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def preset(*n):
		if len(n) == 0:
			inst.query("STATus:PRESet?")
		else:
			inst.query("STATus:PRESet " +str(n[0]))

class questionable:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def condition_2(*n):
		if len(n) == 0:
			inst.query("STATus:QUEStionable:CONDition??")
		else:
			inst.query("STATus:QUEStionable:CONDition? " +str(n[0]))
	def enable(*n):
		if len(n) == 0:
			inst.query("STATus:QUEStionable:ENABle?")
		else:
			inst.query("STATus:QUEStionable:ENABle " +str(n[0]))
	def event(*n):
		if len(n) == 0:
			inst.query("STATus:QUEStionable[:EVENt]??")
		else:
			inst.query("STATus:QUEStionable[:EVENt]? " +str(n[0]))
class status:
	def __init__(self):
		self.operation=operation()
		self.preset=preset()
		self.questionable=questionable()

class beeper:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def state(*n):
		if len(n) == 0:
			inst.query("SYSTem:BEEPer:STATe?")
		else:
			inst.query("SYSTem:BEEPer:STATe " +str(n[0]))

class clear:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def clear(*n):
		if len(n) == 0:
			inst.query("SYSTem:CLEar?")
		else:
			inst.query("SYSTem:CLEar " +str(n[0]))

class communicate:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def aport(*n):
		if len(n) == 0:
			inst.query("SYSTem:COMMunicate:APORt?")
		else:
			inst.query("SYSTem:COMMunicate:APORt " +str(n[0]))
	def brmt(*n):
		if len(n) == 0:
			inst.query("SYSTem:COMMunicate:BRMT?")
		else:
			inst.query("SYSTem:COMMunicate:BRMT " +str(n[0]))
	def gpib_self_address(*n):
		if len(n) == 0:
			inst.query("SYSTem:COMMunicate:GPIB:SELF:ADDRess?")
		else:
			inst.query("SYSTem:COMMunicate:GPIB:SELF:ADDRess " +str(n[0]))
	def lan_self_autoip_state(*n):
		if len(n) == 0:
			inst.query("SYSTem:COMMunicate:LAN:SELF:AUToip:STATe?")
		else:
			inst.query("SYSTem:COMMunicate:LAN:SELF:AUToip:STATe " +str(n[0]))
	def lan_self_dhcp_state(*n):
		if len(n) == 0:
			inst.query("SYSTem:COMMunicate:LAN:SELF:DHCP:STATe?")
		else:
			inst.query("SYSTem:COMMunicate:LAN:SELF:DHCP:STATe " +str(n[0]))
	def lan_self_ip_address(*n):
		if len(n) == 0:
			inst.query("SYSTem:COMMunicate:LAN:SELF:IP:ADDress?")
		else:
			inst.query("SYSTem:COMMunicate:LAN:SELF:IP:ADDress " +str(n[0]))
	def lan_self_ip_dnsserver(*n):
		if len(n) == 0:
			inst.query("SYSTem:COMMunicate:LAN:SELF:IP:DNSServer?")
		else:
			inst.query("SYSTem:COMMunicate:LAN:SELF:IP:DNSServer " +str(n[0]))
	def lan_self_ip_gateway(*n):
		if len(n) == 0:
			inst.query("SYSTem:COMMunicate:LAN:SELF:IP:GATeway?")
		else:
			inst.query("SYSTem:COMMunicate:LAN:SELF:IP:GATeway " +str(n[0]))
	def lan_self_ip_submask(*n):
		if len(n) == 0:
			inst.query("SYSTem:COMMunicate:LAN:SELF:IP:SUBMask?")
		else:
			inst.query("SYSTem:COMMunicate:LAN:SELF:IP:SUBMask " +str(n[0]))
	def lan_self_manuip_state(*n):
		if len(n) == 0:
			inst.query("SYSTem:COMMunicate:LAN:SELF:MANuip:STATe?")
		else:
			inst.query("SYSTem:COMMunicate:LAN:SELF:MANuip:STATe " +str(n[0]))
	def lan_self_reset(*n):
		if len(n) == 0:
			inst.query("SYSTem:COMMunicate:LAN:SELF:RESet?")
		else:
			inst.query("SYSTem:COMMunicate:LAN:SELF:RESet " +str(n[0]))
	def usb_self_address_2(*n):
		if len(n) == 0:
			inst.query("SYSTem:COMMunicate:USB:SELF:ADDRess??")
		else:
			inst.query("SYSTem:COMMunicate:USB:SELF:ADDRess? " +str(n[0]))
	def usb_self_class(*n):
		if len(n) == 0:
			inst.query("SYSTem:COMMunicate:USB:SELF:CLASs?")
		else:
			inst.query("SYSTem:COMMunicate:USB:SELF:CLASs " +str(n[0]))

class configure:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def information_2(*n):
		if len(n) == 0:
			inst.query("SYSTem:CONFigure:INFormation??")
		else:
			inst.query("SYSTem:CONFigure:INFormation? " +str(n[0]))
	def message_2(*n):
		if len(n) == 0:
			inst.query("SYSTem:CONFigure:MESSage??")
		else:
			inst.query("SYSTem:CONFigure:MESSage? " +str(n[0]))

class date:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def date(*n):
		if len(n) == 0:
			inst.query("SYSTem:DATE?")
		else:
			inst.query("SYSTem:DATE " +str(n[0]))

class error:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def next(*n):
		if len(n) == 0:
			inst.query("SYSTem:ERRor[:NEXT]??")
		else:
			inst.query("SYSTem:ERRor[:NEXT]? " +str(n[0]))

class fswitch:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def state(*n):
		if len(n) == 0:
			inst.query("SYSTem:FSWItch:STATe?")
		else:
			inst.query("SYSTem:FSWItch:STATe " +str(n[0]))

class klock:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def klock(*n):
		if len(n) == 0:
			inst.query("SYSTem:KLOCk?")
		else:
			inst.query("SYSTem:KLOCk " +str(n[0]))

class language:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def language(*n):
		if len(n) == 0:
			inst.query("SYSTem:LANGuage?")
		else:
			inst.query("SYSTem:LANGuage " +str(n[0]))

class linemod:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def state_2(*n):
		if len(n) == 0:
			inst.query("SYSTem:LINemod:STATe??")
		else:
			inst.query("SYSTem:LINemod:STATe? " +str(n[0]))
	def type(*n):
		if len(n) == 0:
			inst.query("SYSTem:LINemod:TYPe?")
		else:
			inst.query("SYSTem:LINemod:TYPe " +str(n[0]))

class lkey:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def lkey(*n):
		if len(n) == 0:
			inst.query("SYSTem:LKEY?")
		else:
			inst.query("SYSTem:LKEY " +str(n[0]))

class options_2:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def options_2(*n):
		if len(n) == 0:
			inst.query("SYSTem:OPTions??")
		else:
			inst.query("SYSTem:OPTions? " +str(n[0]))

class pon:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def type(*n):
		if len(n) == 0:
			inst.query("SYSTem:PON:TYPE?")
		else:
			inst.query("SYSTem:PON:TYPE " +str(n[0]))

class preset:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def preset(*n):
		if len(n) == 0:
			inst.query("SYSTem:PRESet?")
		else:
			inst.query("SYSTem:PRESet " +str(n[0]))
	def save(*n):
		if len(n) == 0:
			inst.query("SYSTem:PRESet:SAVE?")
		else:
			inst.query("SYSTem:PRESet:SAVE " +str(n[0]))
	def type(*n):
		if len(n) == 0:
			inst.query("SYSTem:PRESet:TYPE?")
		else:
			inst.query("SYSTem:PRESet:TYPE " +str(n[0]))

class speaker:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def state(*n):
		if len(n) == 0:
			inst.query("SYSTem:SPEaker:STATe?")
		else:
			inst.query("SYSTem:SPEaker:STATe " +str(n[0]))
	def volume(*n):
		if len(n) == 0:
			inst.query("SYSTem:SPEaker:VOLume?")
		else:
			inst.query("SYSTem:SPEaker:VOLume " +str(n[0]))

class time:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def time(*n):
		if len(n) == 0:
			inst.query("SYSTem:TIME?")
		else:
			inst.query("SYSTem:TIME " +str(n[0]))

class tx:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def state_2(*n):
		if len(n) == 0:
			inst.query("SYSTem:TX:STATe??")
		else:
			inst.query("SYSTem:TX:STATe? " +str(n[0]))
	def swset(*n):
		if len(n) == 0:
			inst.query("SYSTem:TX:SWset?")
		else:
			inst.query("SYSTem:TX:SWset " +str(n[0]))
	def swsta_2(*n):
		if len(n) == 0:
			inst.query("SYSTem:TX:SWSTa??")
		else:
			inst.query("SYSTem:TX:SWSTa? " +str(n[0]))

class userkey:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def confirm(*n):
		if len(n) == 0:
			inst.query("SYSTem:USERkey:CONFirm?")
		else:
			inst.query("SYSTem:USERkey:CONFirm " +str(n[0]))
	def keycmd(*n):
		if len(n) == 0:
			inst.query("SYSTem:USERkey:KEYCmd?")
		else:
			inst.query("SYSTem:USERkey:KEYCmd " +str(n[0]))
	def state(*n):
		if len(n) == 0:
			inst.query("SYSTem:USERkey:STATe?")
		else:
			inst.query("SYSTem:USERkey:STATe " +str(n[0]))

class version_2:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def version_2(*n):
		if len(n) == 0:
			inst.query("SYSTem:VERSion??")
		else:
			inst.query("SYSTem:VERSion? " +str(n[0]))
class system:
	def __init__(self):
		self.beeper=beeper()
		self.clear=clear()
		self.communicate=communicate()
		self.configure=configure()
		self.date=date()
		self.error=error()
		self.fswitch=fswitch()
		self.klock=klock()
		self.language=language()
		self.linemod=linemod()
		self.lkey=lkey()
		self.options?=options_2()
		self.pon=pon()
		self.preset=preset()
		self.speaker=speaker()
		self.time=time()
		self.tx=tx()
		self.userkey=userkey()
		self.version?=version_2()

class average:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def clear(*n):
		if len(n) == 0:
			inst.query("TRACe:AVERage:CLEar?")
		else:
			inst.query("TRACe:AVERage:CLEar " +str(n[0]))
	def count(*n):
		if len(n) == 0:
			inst.query("TRACe:AVERage:COUNt?")
		else:
			inst.query("TRACe:AVERage:COUNt " +str(n[0]))
	def count_current_2(*n):
		if len(n) == 0:
			inst.query("TRACe:AVERage:COUNt:CURRent??")
		else:
			inst.query("TRACe:AVERage:COUNt:CURRent? " +str(n[0]))
	def reset(*n):
		if len(n) == 0:
			inst.query("TRACe:AVERage:RESet?")
		else:
			inst.query("TRACe:AVERage:RESet " +str(n[0]))

class clear:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def all(*n):
		if len(n) == 0:
			inst.query("TRACe:CLEar:ALL?")
		else:
			inst.query("TRACe:CLEar:ALL " +str(n[0]))

class data:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def data(*n):
		if len(n) == 0:
			inst.query("TRACe:DATA?")
		else:
			inst.query("TRACe:DATA " +str(n[0]))

class math:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def a(*n):
		if len(n) == 0:
			inst.query("TRACe:MATH:A?")
		else:
			inst.query("TRACe:MATH:A " +str(n[0]))
	def b(*n):
		if len(n) == 0:
			inst.query("TRACe:MATH:B?")
		else:
			inst.query("TRACe:MATH:B " +str(n[0]))
	def const(*n):
		if len(n) == 0:
			inst.query("TRACe:MATH:CONSt?")
		else:
			inst.query("TRACe:MATH:CONSt " +str(n[0]))
	def peak_data(*n):
		if len(n) == 0:
			inst.query("TRACe:MATH:PEAK[:DATA]??")
		else:
			inst.query("TRACe:MATH:PEAK[:DATA]? " +str(n[0]))
	def peak_points_2(*n):
		if len(n) == 0:
			inst.query("TRACe:MATH:PEAK:POINts??")
		else:
			inst.query("TRACe:MATH:PEAK:POINts? " +str(n[0]))
	def peak_sort(*n):
		if len(n) == 0:
			inst.query("TRACe:MATH:PEAK:SORT?")
		else:
			inst.query("TRACe:MATH:PEAK:SORT " +str(n[0]))
	def peak_table_state(*n):
		if len(n) == 0:
			inst.query("TRACe:MATH:PEAK:TABLe:STATe?")
		else:
			inst.query("TRACe:MATH:PEAK:TABLe:STATe " +str(n[0]))
	def peak_threshold(*n):
		if len(n) == 0:
			inst.query("TRACe:MATH:PEAK:THReshold?")
		else:
			inst.query("TRACe:MATH:PEAK:THReshold " +str(n[0]))
	def state(*n):
		if len(n) == 0:
			inst.query("TRACe:MATH:STATe?")
		else:
			inst.query("TRACe:MATH:STATe " +str(n[0]))
	def type(*n):
		if len(n) == 0:
			inst.query("TRACe:MATH:TYPE?")
		else:
			inst.query("TRACe:MATH:TYPE " +str(n[0]))
class trace:
	def __init__(self):
		self.average=average()
		self.clear=clear()
		self.data=data()
		self.math=math()

class average:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def type(*n):
		if len(n) == 0:
			inst.query("TRACe<n>:AVERage:TYPE?")
		else:
			inst.query("TRACe<n>:AVERage:TYPE " +str(n[0]))

class mode:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def mode(*n):
		if len(n) == 0:
			inst.query("TRACe<n>:MODE?")
		else:
			inst.query("TRACe<n>:MODE " +str(n[0]))
class trace<n>:
	def __init__(self):
		self.average=average()
		self.mode=mode()

class sequence:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def external_ready_2(*n):
		if len(n) == 0:
			inst.query("TRIGger:SEQuence:EXTernal:READy??")
		else:
			inst.query("TRIGger:SEQuence:EXTernal:READy? " +str(n[0]))
	def external_slope(*n):
		if len(n) == 0:
			inst.query("TRIGger:SEQuence:EXTernal:SLOPe?")
		else:
			inst.query("TRIGger:SEQuence:EXTernal:SLOPe " +str(n[0]))
	def source(*n):
		if len(n) == 0:
			inst.query("TRIGger:SEQuence:SOURce?")
		else:
			inst.query("TRIGger:SEQuence:SOURce " +str(n[0]))
	def video_level(*n):
		if len(n) == 0:
			inst.query("TRIGger:SEQuence:VIDeo:LEVel?")
		else:
			inst.query("TRIGger:SEQuence:VIDeo:LEVel " +str(n[0]))

class power:
	def __init__(self):
		INFO_NO_PARAM="no admite parametro, se envia consulta"
	def power(*n):
		if len(n) == 0:
			inst.query("UNIT:POWer?")
		else:
			inst.query("UNIT:POWer " +str(n[0]))
class unit:
	def __init__(self):
		self.sequence=sequence()
		self.power=power()
class rigol:
	def __init__(self):
		self.calculate=calculate()
		self.calibration=calibration()
		self.CONFigure?=CONFigure_2()
		self.configure=configure()
		self.COUPle=COUPle()
		self.display=display()
		self.fetch=fetch()
		self.format=format()
		self.hcopy=hcopy()
		self.initiate=initiate()
		self.input=input()
		self.mmemory=mmemory()
		self.output=output()
		self.read=read()
		self.sense=sense()
		self.source=source()
		self.status=status()
		self.system=system()
		self.trace=trace()
		self.trace<n>=trace<n>()
		self.trigger=trigger()
