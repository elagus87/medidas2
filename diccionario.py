
# coding: utf-8

# # Ejemplo de diccionarios
# El diccionario es un listado similar al vector pero indexado a traves de palabras
# Se hace uso de tuplas:
# - conjunto de valores de cualquier tipo
# - el primer "valor", pr ej identifier, es la KEY
# - el segundo es simplemente VALUE, en este caso una tupla
# - las tuplas de 2 componentes se conocen como PAIR
# 
# Todo el ejemplo se basa en el proyecto de control de equipos de medicion

# In[9]:


dicc_IEEE={'Q_identifier'            :('Q','*IDN',False),
           'W_reset'                 :('W','*RST' ,False),
           'Q_operation_complete'    :('Q','*OPC',False),
           'Q_service_request_enable':('Q','*SRE',True),
           'W_clear_status'          :('W','*CLS',False),
           'Q_event_status_register' :('Q','*ESR',True),
           'Q_status_byte'           :('Q','*STB',True),
           'Q_event_status_en'       :('Q','*ESE',True)}

print(dicc_IEEE['Q_identifier'][0])
print(dicc_IEEE['Q_identifier'][1])


# ### Funcion de conformacion de mensaje
# En base al diccionario conformo un mensaje de salida:
# 1. si admite parametro (true) debera ver si recibe o no el mismo
# 2. si no admite parametro, retorna el comando
# 3. las iniciadas en Q esperan respuesta, las inidiadas en W, solo escriben

# In[10]:


def build_command ( command, *parameter ):
    if (dicc_IEEE [command][1]): #tiene true
        if len(parameter) == 0:
            return (dicc_IEEE[command][1] + '?')
        else: 
            return (dicc_IEEE[command][1] + ' ' + str(*parameter))
    else:
        if len(parameter) != 0:
            print('el comando no admite parametro: parametro descartado \n')
        return (dicc_IEEE[command][1])


# In[11]:


print(build_command('Q_identifier'))
print(build_command('W_reset'))
print(build_command('Q_service_request_enable'))
print(build_command('Q_service_request_enable',8))
print(build_command('W_reset',7))


# ## mejora de envio de datos directo con PyVisa
# Para el control de equipos se hace uso de PyVisa. Las pruebas requieren el equipo conectado

# In[12]:


import visa


# In[20]:


#rm = visa.ResourceManager()
#a = rm.list_resources()
#inst = rm.open_resource(a[0])
#inst.write("*IDN?")
class inst:
    def write(command):
        print('enviado '+command)
    def query(command):
        print('enviado '+command)
        print('   recibido: bien')
    def read(command):
        print('recibido '+command)


# In[21]:


# # funcion que automatiza el envio
# ## el mismo comando impone si se debe enviar, recibir o ambos
def cmd(command,*parameter):
    if dicc_IEEE[command][0] == 'Q':
        inst.query(build_command(command,*parameter))
    elif dicc_IEEE[command][0] == 'W':
        inst.write(build_command(command,*parameter))
    else:
        inst.read(build_command(command,*parameter))

        


# In[23]:


cmd('W_reset')
cmd('Q_service_request_enable')
cmd('Q_service_request_enable',8)

