LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

entity ffd is
generic(W:natural:=8);
  port (
	  d :in std_logic;
	clk :in std_logic;
    rst :in std_logic;
	  q :out std_logic) ;
end ffd ;

architecture Behavioral of ffd is
begin
	process ( clk , rst )
		begin
		if (rst = '1') then
			q <= '1';
		elsif( rising_edge (clk) ) then
			q <= d;
		end if ;
	end process ;    
end architecture;