#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Sat Jul 9 15:17:24 2019

@author: mariano
"""

# Importing the requests library 
import os, sys, inspect
from mult_replace import mult_replace


# Nombre del archivo que contiene las funciones del powermeter
filename 	= "man_pwmt_anritsu_ml2487b.txt"
out_filename 	= "prueba_output.py"

DEBUG 		= True

params_old 	= ["<c>", "<s>", "<unit_type>"]
params_new 	= ["<chnl=1>", "<sensor=\"A\">", "<unit_type=\"DB\">"]



LINE_DIV = "-----------------------------------------------------------------\n"

print()
print(LINE_DIV)
print()
print("--------------------------   ML248xB   --------------------------\n")
print("-----------------   Wideband Peak Power Meter  ------------------\n")
print()
print(LINE_DIV)

if len(params_old) != len(params_new):
	print("!!! Error, la longitud de los parámetros a reemplazar no coincide.")
	exit()

# The file is readed and then the functions' info is extracted
with open(filename, 'r') as f:

	read_data = f.read()

	# Data was previously formated in order to find easily the keys
	commands_info = read_data.split("^")
	commands_keys = read_data.split(" Command: ")
	
	# Only the function's prototype is stored as key
	for i in range(len(commands_keys)):
		commands_keys[i] = commands_keys[i].split("\n")[0]
	
	# A dictionary is created with the keys and the commands info
	commands_dic = dict(zip(commands_keys,commands_info))

	del commands_dic[list(commands_dic.keys())[0]]
	
result = False

if len(sys.argv) > 1:

	print("Resultados para: \t\""+sys.argv[1]+"\"\n")

	i = 0

	for key in commands_dic.keys():

		if sys.argv[1] in key:

			i+=1

			print("***\t"+str(i)+")\n")

			print(commands_dic[key])

if DEBUG:
	print("")

	if result:
		print(commands_dic[result])
	else:
		for i, item in zip(range(len(commands_dic)), commands_dic):
			print(str(i).zfill(3)+" - "+item)

	print("")

prev_key = "null"

state_val = 0

with open(out_filename,"w") as f:

	with open("header_pwmt.txt","r")  as rf:
		f.write(rf.read()+"\n")
	with open("imports_pwmt.txt","r") as rf:
		f.write(rf.read()+"\n")
	with open("class_pwmt.txt","r")   as rf:
		f.write(rf.read()+"\n")

	with open("query_write_code.txt","r") as rf:
		query_write_code = rf.read()
	with open("query_only_code.txt","r")  as rf:
		query_only_code  = rf.read()
	with open("write_only_code.txt","r")  as rf:
		write_only_code  = rf.read()

	for key in commands_dic.keys():

		# Lo de .split es para eliminar los parámetros si los hubiera y el replace elimita caracteres inválidos
		if prev_key.split("<")[0] == key.split("?")[0]:

			f.write("\t")

			f.write("def "+prev_key.split("<")[0].replace("*","").replace("?","")+"(self,cmd_type=\"QUERY\"")

			for i,param in zip(range(len(prev_key.split("<"))), mult_replace(prev_key,params_old,params_new).split("<")):
				if i > 1:
					if i == 2:
						f.write(",")
					f.write(param.split(">")[0])

			f.write("):\n")
			
			f.write(query_write_code+"\n\n")

			state_val = 1

		elif state_val > 1:

			f.write("\t")

			f.write("def "+prev_key.split("<")[0].replace("*","").replace("?",""))

			if ("Query Command:" in commands_dic[prev_key].split("\n")[0]):
				f.write("(self,cmd_type=\"QUERY\"")
			else:
				f.write("(self,cmd_type=\"WRITE\"")

			for i,param in zip(range(len(prev_key.split("<"))), mult_replace(prev_key,params_old,params_new).split("<")):
				if i > 1:
					if i == 2:
						f.write(",")
					f.write(param.split(">")[0])
			f.write("):\n")

			if "Query Command" in commands_dic[prev_key]:
				f.write(query_only_code+"\n\n")
			else:
				f.write(write_only_code+"\n\n")

			state_val = 2

		else:
			state_val = 3

		prev_key = key


	if state_val > 1:

		f.write("\t")

		f.write("def "+prev_key.split("<")[0].replace("*","").replace("?",""))

		if ("Query Command:" in commands_dic[prev_key].split("\n")[0]):
			f.write("(self,cmd_type=\"QUERY\"")
		else:
			f.write("(self,cmd_type=\"WRITE\"")

		for i,param in zip(range(len(prev_key.split("<"))), mult_replace(prev_key,params_old,params_new).split("<")):
			if i > 1:
				if i == 2:
					f.write(",")
				f.write(param.split(">")[0])

		f.write("):\n")

		if "Query Command" in commands_dic[prev_key]:
			f.write(query_only_code+"\n\n")
		else:
			f.write(write_only_code+"\n\n")

