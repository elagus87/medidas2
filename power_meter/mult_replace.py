#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def mult_replace(str_old,list_find,list_replace):
	str_new = str_old
	
	for x, y in zip(list_find, list_replace):
		str_new = str_new.replace(x,y)		

	return str_new
