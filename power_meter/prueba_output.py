#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Importing the requested library 
import os

class PowerMeterAnritsuML2487B:

	def __init__(self, addr="192.168.0.2"):
		self.addr = addr

	def set_addr(self, addr="192.168.0.2"):
		self.addr = addr

	def pwmt_com(self,command="*IDN?",cmd_type="WRITE"):

		# Anritsu VISA FORM's URL
		URL = "http://"+self.addr+"/ctl.html"

		# Hides Curl's progress 
		SILENT = "-s"

		# Curl's command
		curl_cmd = "curl '" + URL + "' --data 'cm=" + command + "+&q=" + cmd_type + "' " + SILENT

		# Gets Curl's response (Raw HTML)
		response = os.popen(curl_cmd).read()

		# Formats Curl's response to get the "answer"
		answer = response.split('<textarea name="qr" id="qr" ')[1].split('</textarea')[0].split('>')[1]

		return answer

	def CLS(self,cmd_type="WRITE"):
		#EMPTY


	def ESE(self,cmd_type="QUERY",mask):
		#EMPTY-/?

		return str_ans


	def ESR(self,cmd_type="QUERY"):
		#EMPTY/?

		return str_ans


	def IDN(self,cmd_type="QUERY"):
		#EMPTY/?

		return str_ans


	def OPC(self,cmd_type="QUERY"):
		#EMPTY-/?

		return str_ans


	def RST(self,cmd_type="WRITE"):
		#EMPTY


	def SRE(self,cmd_type="QUERY",mask):
		#EMPTY-/?

		return str_ans


	def STB(self,cmd_type="QUERY"):
		#EMPTY/?

		return str_ans


	def TRG(self,cmd_type="WRITE"):
		#EMPTY/?

		return str_ans


	def TST(self,cmd_type="QUERY"):
		#EMPTY/?

		return str_ans


	def WAI(self,cmd_type="WRITE"):
		#EMPTY


	def GT0(self,cmd_type="WRITE"):
		#EMPTY


	def GT1(self,cmd_type="WRITE"):
		#EMPTY


	def GT2(self,cmd_type="WRITE"):
		#EMPTY


	def TR0(self,cmd_type="WRITE"):
		#EMPTY


	def TR1(self,cmd_type="WRITE",chnl=1):
		#EMPTY


	def TR2(self,cmd_type="WRITE",chnl=1):
		#EMPTY


	def TR3(self,cmd_type="WRITE"):
		#EMPTY


	def CHACTIV(self,cmd_type="QUERY",chnl=1):
		#EMPTY-/?

		return str_ans


	def CHCFG(self,cmd_type="QUERY",chnl=1,config):
		#EMPTY-/?

		return str_ans


	def CHDISPN(self,cmd_type="QUERY",num_channels):
		#EMPTY-/?

		return str_ans


	def CHMODE(self,cmd_type="QUERY",chnl=1,mode):
		#EMPTY-/?

		return str_ans


	def CHRES(self,cmd_type="QUERY",chnl=1,dec_places):
		#EMPTY-/?

		return str_ans


	def CHUNIT(self,cmd_type="QUERY",chnl=1,units):
		#EMPTY-/?

		return str_ans


	def CWSETLP(self,cmd_type="QUERY",chnl=1,settle_pct):
		#EMPTY-/?

		return str_ans


	def PMDTYP(self,cmd_type="QUERY",chnl=1,meas_type):
		#EMPTY-/?

		return str_ans


	def PMMEAS(self,cmd_type="QUERY",chnl=1,meas_type_num):
		#EMPTY-/?

		return str_ans


	def PMRRS(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def TRARMD(self,cmd_type="QUERY",chnl=1,meas_mode,arm_mode):
		#EMPTY-/?

		return str_ans


	def TRAUTOS(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def TRBW(self,cmd_type="QUERY",chnl=1,bandwidth):
		#EMPTY-/?

		return str_ans


	def TRCAPT(self,cmd_type="QUERY",chnl=1,meas_mode,time,units):
		#EMPTY-/?

		return str_ans


	def TRDLYT(self,cmd_type="QUERY",chnl=1,meas_mode,time,units):
		#EMPTY-/?

		return str_ans


	def TRFLEV(self,cmd_type="QUERY",chnl=1,frm_level):
		#EMPTY-/?

		return str_ans


	def TRFTIM(self,cmd_type="QUERY",chnl=1,frm_duration):
		#EMPTY-/?

		return str_ans


	def TRHOFS(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def TRHOFT(self,cmd_type="QUERY",chnl=1,holdoff_time):
		#EMPTY-/?

		return str_ans


	def TRINEDG(self,cmd_type="QUERY",chnl=1,meas_mode,edge):
		#EMPTY-/?

		return str_ans


	def TRINLEV(self,cmd_type="QUERY",chnl=1,meas_mode,pw_lev):
		#EMPTY-/?

		return str_ans


	def TRLINKS(self,cmd_type="QUERY",state):
		#EMPTY-/?

		return str_ans


	def TRSAMPL(self,cmd_type="QUERY",chnl=1,sample_rate):
		#EMPTY-/?

		return str_ans


	def TRSRC(self,cmd_type="QUERY",chnl=1,meas_mode,source):
		#EMPTY-/?

		return str_ans


	def TRWFPOS(self,cmd_type="WRITE",chnl=1,position):
		#EMPTY


	def TRWFS(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def TRXEDG(self,cmd_type="QUERY",edge):
		#EMPTY-/?

		return str_ans


	def GP1REPN(self,cmd_type="QUERY",chnl=1,repeat_number):
		#EMPTY-/?

		return str_ans


	def GP1REPS(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def GP1REPT(self,cmd_type="QUERY",chnl=1,time):
		#EMPTY-/?

		return str_ans


	def GPACTN(self,cmd_type="QUERY",chnl=1,gp):
		#EMPTY-/?

		return str_ans


	def GPAMO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def GPRST(self,cmd_type="WRITE",chnl=1):
		#EMPTY


	def GPFENS(self,cmd_type="QUERY",chnl=1,gp,state):
		#EMPTY-/?

		return str_ans


	def GPFENSP(self,cmd_type="QUERY",chnl=1,gp,time):
		#EMPTY-/?

		return str_ans


	def GPFENST(self,cmd_type="QUERY",chnl=1,gp,time):
		#EMPTY-/?

		return str_ans


	def GPGATS(self,cmd_type="QUERY",chnl=1,gp,state):
		#EMPTY-/?

		return str_ans


	def GPHIDES(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def GPMO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def GPNMO(self,cmd_type="QUERY",chnl=1,gp_num):
		#EMPTY/?

		return str_ans


	def GPOFF(self,cmd_type="WRITE",chnl=1):
		#EMPTY


	def GPTIMSP(self,cmd_type="QUERY",chnl=1,gp,time):
		#EMPTY-/?

		return str_ans


	def GPTIMST(self,cmd_type="QUERY",chnl=1,gp,time):
		#EMPTY-/?

		return str_ans


	def CWREL(self,cmd_type="QUERY",chnl=1,mode):
		#EMPTY-/?

		return str_ans


	def CWAVG(self,cmd_type="QUERY",chnl=1,mode,avg_num):
		#EMPTY-/?

		return str_ans


	def PMAVGN(self,cmd_type="QUERY",chnl=1,value):
		#EMPTY-/?

		return str_ans


	def PMAVGS(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def PMAVRST(self,cmd_type="WRITE",chnl=1):
		#EMPTY


	def PMPDRST(self,cmd_type="WRITE",chnl=1):
		#EMPTY


	def CWDUTY(self,cmd_type="QUERY",chnl=1,duty_pct):
		#EMPTY-/?

		return str_ans


	def CWDUTYS(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def MKACTN(self,cmd_type="QUERY",chnl=1,marker_num):
		#EMPTY-/?

		return str_ans


	def MKACTO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def MKAOFF(self,cmd_type="WRITE",chnl=1):
		#EMPTY


	def MKAPOS(self,cmd_type="QUERY",chnl=1,time,units):
		#EMPTY-/?

		return str_ans


	def MKDELTS(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def MKDLINK(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def MKDMEAS(self,cmd_type="QUERY",chnl=1,meas_type):
		#EMPTY-/?

		return str_ans


	def MKDO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def MKDPOS(self,cmd_type="QUERY",chnl=1,time,units):
		#EMPTY-/?

		return str_ans


	def MKENO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def MKNO(self,cmd_type="QUERY",chnl=1,mk_num):
		#EMPTY/?

		return str_ans


	def MKPFTO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def MKPOS(self,cmd_type="QUERY",chnl=1,marker_num,time,units):
		#EMPTY-/?

		return str_ans


	def MKPOTO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def MKPRIO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def MKPRTO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def MKPSLT(self,cmd_type="QUERY",chnl=1,value):
		#EMPTY-/?

		return str_ans


	def MKPSSV(self,cmd_type="QUERY",chnl=1,source):
		#EMPTY-/?

		return str_ans


	def MKPSUT(self,cmd_type="QUERY",chnl=1,value):
		#EMPTY-/?

		return str_ans


	def MKPWTO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def MKSTATE(self,cmd_type="QUERY",chnl=1,marker_num,state):
		#EMPTY-/?

		return str_ans


	def MKTMAX(self,cmd_type="WRITE",chnl=1):
		#EMPTY


	def MKTMIN(self,cmd_type="WRITE",chnl=1):
		#EMPTY


	def LMFBEEP(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def LMFCLR(self,cmd_type="WRITE",chnl=1):
		#EMPTY


	def LMFHOLD(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def LMLINE(self,cmd_type="QUERY",chnl=1,limit_line):
		#EMPTY-/?

		return str_ans


	def LMSLO(self,cmd_type="QUERY",chnl=1,limit_val):
		#EMPTY-/?

		return str_ans


	def LMSTATE(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def LMSUP(self,cmd_type="QUERY",chnl=1,limit_val):
		#EMPTY-/?

		return str_ans


	def LMTYP(self,cmd_type="QUERY",chnl=1,type):
		#EMPTY-/?

		return str_ans


	def LMXASTQ(self,cmd_type="QUERY"):
		#EMPTY/?

		return str_ans


	def LMXNAME(self,cmd_type="QUERY",store_num,name_str):
		#EMPTY-/?

		return str_ans


	def LMXPOF(self,cmd_type="QUERY",chnl=1,offset):
		#EMPTY-/?

		return str_ans


	def LMXREPN(self,cmd_type="QUERY",chnl=1,count):
		#EMPTY-/?

		return str_ans


	def LMXREPS(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def LMXROFP(self,cmd_type="QUERY",chnl=1,offset):
		#EMPTY-/?

		return str_ans


	def LMXROFT(self,cmd_type="QUERY",chnl=1,offset):
		#EMPTY-/?

		return str_ans


	def LMXSAVE(self,cmd_type="WRITE"):
		#EMPTY


	def LMXSEG(self,cmd_type="WRITE",seg_limits):
		#EMPTY


	def LMXSID(self,cmd_type="WRITE",store_num,name_str):
		#EMPTY


	def LMXSPEC(self,cmd_type="QUERY",chnl=1,spec_category,spec_number):
		#EMPTY-/?

		return str_ans


	def LMXSPEF(self,cmd_type="WRITE",store_num,name_str,num_seg,seg_data):
		#EMPTY


	def LMXSPO(self,cmd_type="QUERY",store_category,store_num):
		#EMPTY/?

		return str_ans


	def LMXSTQ(self,cmd_type="QUERY",store_num):
		#EMPTY/?

		return str_ans


	def LMXTOF(self,cmd_type="QUERY",chnl=1,offset):
		#EMPTY-/?

		return str_ans


	def PMPAUTO(self,cmd_type="WRITE",chnl=1):
		#EMPTY


	def PMPREF(self,cmd_type="QUERY",chnl=1,unit_type="DB",ref_level,suffix_mult,suffix_unit):
		#EMPTY-/?

		return str_ans


	def PMPSCAL(self,cmd_type="QUERY",chnl=1,unit_type="DB",scale_value,suffix_mult,suffix_unit):
		#EMPTY-/?

		return str_ans


	def CWMMRST(self,cmd_type="WRITE",chnl=1):
		#EMPTY


	def CWMMTKS(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def PMPDREP(self,cmd_type="QUERY",chnl=1,type):
		#EMPTY-/?

		return str_ans


	def PMPTRK(self,cmd_type="QUERY",chnl=1,mode):
		#EMPTY-/?

		return str_ans


	def CHOLD(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def CHPIRST(self,cmd_type="WRITE",chnl=1):
		#EMPTY


	def CHPKS(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def PPACQRT(self,cmd_type="WRITE",chnl=1):
		#EMPTY


	def PPACQS(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def PPFUNC(self,cmd_type="QUERY",chnl=1,module):
		#EMPTY-/?

		return str_ans


	def TTFRO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def TTFUNC(self,cmd_type="QUERY",chnl=1,function):
		#EMPTY-/?

		return str_ans


	def TTMKPOS(self,cmd_type="QUERY",chnl=1,position):
		#EMPTY-/?

		return str_ans


	def TTMKRO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def TTMKS(self,cmd_type="QUERY",chnl=1,state):
		#EMPTY-/?

		return str_ans


	def TTPSP(self,cmd_type="QUERY",chnl=1,power):
		#EMPTY-/?

		return str_ans


	def TTPST(self,cmd_type="QUERY",chnl=1,power):
		#EMPTY-/?

		return str_ans


	def TTSRC(self,cmd_type="QUERY",chnl=1,source):
		#EMPTY-/?

		return str_ans


	def TTZIN(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def TTZOUT(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def PAEBI(self,cmd_type="QUERY",chnl=1,current):
		#EMPTY-/?

		return str_ans


	def PAEBICF(self,cmd_type="QUERY",chnl=1,factor):
		#EMPTY-/?

		return str_ans


	def PAEBIS(self,cmd_type="QUERY",chnl=1,source):
		#EMPTY-/?

		return str_ans


	def PAEBV(self,cmd_type="QUERY",chnl=1,volts):
		#EMPTY-/?

		return str_ans


	def PAECFG(self,cmd_type="QUERY",chnl=1,config):
		#EMPTY-/?

		return str_ans


	def PAEO(self,cmd_type="WRITE",chnl=1):
		#EMPTY


	def PAESRC(self,cmd_type="QUERY",chnl=1,source):
		#EMPTY-/?

		return str_ans


	def SNFILTS(self,cmd_type="QUERY",sensor="A",state):
		#EMPTY-/?

		return str_ans


	def SNTYPE(self,cmd_type="QUERY",sensor="A"):
		#EMPTY/?

		return str_ans


	def SNUNIVM(self,cmd_type="QUERY",sensor="A",mode):
		#EMPTY-/?

		return str_ans


	def SNCFADJ(self,cmd_type="QUERY",sensor="A",units,val):
		#EMPTY-/?

		return str_ans


	def SNCFCAL(self,cmd_type="QUERY",sensor="A",units,val):
		#EMPTY-/?

		return str_ans


	def SNCFRQ(self,cmd_type="QUERY",sensor="A",value,units):
		#EMPTY-/?

		return str_ans


	def SNCFSRC(self,cmd_type="QUERY",sensor="A",source):
		#EMPTY-/?

		return str_ans


	def SNCFU(self,cmd_type="QUERY",units):
		#EMPTY-/?

		return str_ans


	def SCFVAL(self,cmd_type="QUERY",sensor="A"):
		#EMPTY/?

		return str_ans


	def SNZSPF(self,cmd_type="QUERY",sensor="A",freq,units):
		#EMPTY-/?

		return str_ans


	def SNZSPV(self,cmd_type="QUERY",sensor="A",volt,units):
		#EMPTY-/?

		return str_ans


	def SNZSTF(self,cmd_type="QUERY",sensor="A",freq,units):
		#EMPTY-/?

		return str_ans


	def SNZSTV(self,cmd_type="QUERY",sensor="A",volt,units):
		#EMPTY-/?

		return str_ans


	def SNOFIX(self,cmd_type="QUERY",sensor="A",fix_offset,units):
		#EMPTY-/?

		return str_ans


	def SNOFTYP(self,cmd_type="QUERY",sensor="A",type):
		#EMPTY-/?

		return str_ans


	def SNOFVO(self,cmd_type="QUERY",sensor="A"):
		#EMPTY/?

		return str_ans


	def SNOTAO(self,cmd_type="QUERY",table_num):
		#EMPTY/?

		return str_ans


	def SNOTAW(self,cmd_type="QUERY"):
		#EMPTY/?

		return str_ans


	def SNOTADD(self,cmd_type="QUERY",table_num,freq,suffix_mult,suffix_unit,offset):
		#EMPTY/?

		return str_ans


	def SNTOBO(self,cmd_type="QUERY",table_num):
		#EMPTY/?

		return str_ans


	def SNOTBW(self,cmd_type="WRITE",table_num,num_bytes):
		#EMPTY


	def SNOTCLR(self,cmd_type="WRITE",table_num):
		#EMPTY


	def SNOTID(self,cmd_type="QUERY",table_num,id_string):
		#EMPTY-/?

		return str_ans


	def SNOTSEL(self,cmd_type="QUERY",sensor="A",table_num):
		#EMPTY-/?

		return str_ans


	def SNOTVLD(self,cmd_type="QUERY",table_num):
		#EMPTY/?

		return str_ans


	def SNCFUSE(self,cmd_type="QUERY",sensor="A"):
		#EMPTY/?

		return str_ans


	def SNCTABN(self,cmd_type="WRITE",sensor="A",table_number):
		#EMPTY


	def SNCTADD(self,cmd_type="WRITE",sensor="A",table_number,frequency_value,units,cal_factor,cal_factor_units):
		#EMPTY


	def SNCTAO(self,cmd_type="QUERY",sensor="A",table_number):
		#EMPTY/?

		return str_ans


	def SNCTAW(self,cmd_type="WRITE",sensor="A",table_number,id_string,num_entry_pairs,ascii_data):
		#EMPTY


	def SNCTBIN(self,cmd_type="WRITE",sensor="A",table_number,length,binary_data):
		#EMPTY


	def SNCTBO(self,cmd_type="WRITE",sensor="A",table_number):
		#EMPTY


	def SNCTCLR(self,cmd_type="WRITE",sensor="A",table_number):
		#EMPTY


	def SNCTID(self,cmd_type="QUERY",sensor="A",table_number,id_string):
		#EMPTY-/?

		return str_ans


	def SNCTNQ(self,cmd_type="WRITE",sensor="A"):
		#EMPTY


	def SNCTPRE(self,cmd_type="WRITE",sensor="A",table_number):
		#EMPTY


	def SNCTSAV(self,cmd_type="WRITE"):
		#EMPTY


	def SNCTVAL(self,cmd_type="WRITE",sensor="A",table_number):
		#EMPTY


	def SNRGH(self,cmd_type="QUERY",sensor="A",range):
		#EMPTY-/?

		return str_ans


	def BNVZERO(self,cmd_type="WRITE"):
		#EMPTY


	def SNCAL(self,cmd_type="WRITE",sensor="A"):
		#EMPTY


	def SNCALF(self,cmd_type="QUERY",cal_frq):
		#EMPTY-/?

		return str_ans


	def SNRFCAL(self,cmd_type="QUERY",state):
		#EMPTY-/?

		return str_ans


	def SNZERO(self,cmd_type="WRITE",sensor="A"):
		#EMPTY


	def RCL(self,cmd_type="WRITE",store):
		#EMPTY


	def SAV(self,cmd_type="WRITE",store):
		#EMPTY


	def NVLOAD(self,cmd_type="WRITE",store_number,data_length,binary_data):
		#EMPTY


	def NVNAME(self,cmd_type="QUERY",store_number,store_name):
		#EMPTY-/?

		return str_ans


	def NVOUT(self,cmd_type="QUERY",store_number):
		#EMPTY/?

		return str_ans


	def BNC1M(self,cmd_type="QUERY",mode):
		#EMPTY-/?

		return str_ans


	def BNC2M(self,cmd_type="QUERY",mode):
		#EMPTY-/?

		return str_ans


	def BNCDSP(self,cmd_type="WRITE",bnc,units,power):
		#EMPTY


	def BNDSP(self,cmd_type="QUERY",bnc,units):
		#EMPTY/?

		return str_ans


	def BNDST(self,cmd_type="QUERY",bnc,units,power):
		#EMPTY-/?

		return str_ans


	def BNOCH(self,cmd_type="QUERY",bnc,channel):
		#EMPTY-/?

		return str_ans


	def BNPLEV(self,cmd_type="QUERY",port,volt_level):
		#EMPTY-/?

		return str_ans


	def BNVOSP(self,cmd_type="QUERY",bnc,volts):
		#EMPTY-/?

		return str_ans


	def BNVOST(self,cmd_type="QUERY",bnc,volts):
		#EMPTY-/?

		return str_ans


	def SYADDR(self,cmd_type="WRITE",val):
		#EMPTY


	def SYBAUD(self,cmd_type="QUERY",baud_rate):
		#EMPTY-/?

		return str_ans


	def SYBEEPS(self,cmd_type="QUERY",state):
		#EMPTY-/?

		return str_ans


	def SYBUFS(self,cmd_type="QUERY",state):
		#EMPTY-/?

		return str_ans


	def SYDLIT(self,cmd_type="QUERY",setting):
		#EMPTY-/?

		return str_ans


	def SYDRES(self,cmd_type="QUERY",num_points):
		#EMPTY-/?

		return str_ans


	def SYIMAGE(self,cmd_type="WRITE"):
		#EMPTY


	def SYLUT(self,cmd_type="WRITE"):
		#EMPTY


	def SYSTEP(self,cmd_type="QUERY",unit_type="DB",value,suffix_mult,suffix_unit):
		#EMPTY-/?

		return str_ans


	def SYTACTS(self,cmd_type="QUERY",state):
		#EMPTY-/?

		return str_ans


	def SYTEXT(self,cmd_type="QUERY",text_string):
		#EMPTY-/?

		return str_ans


	def SYTEXTS(self,cmd_type="QUERY",state):
		#EMPTY-/?

		return str_ans


	def NVSECS(self,cmd_type="QUERY",state):
		#EMPTY-/?

		return str_ans


	def SYOI(self,cmd_type="QUERY"):
		#EMPTY/?

		return str_ans


	def NVAPN(self,cmd_type="QUERY",store_num):
		#EMPTY-/?

		return str_ans


	def NVFRST(self,cmd_type="WRITE"):
		#EMPTY


	def CWO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def CWON(self,cmd_type="QUERY",chnl=1,num_readings):
		#EMPTY/?

		return str_ans


	def PMNPBLO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def PMNPBO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def PMNPO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def PMPBLO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def PMPBO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def PMPO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def PMRDO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def PMXPBLO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def PMXPBO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def PMXPO(self,cmd_type="QUERY",chnl=1):
		#EMPTY/?

		return str_ans


	def SYCONT(self,cmd_type="WRITE"):
		#EMPTY


	def SYDISP(self,cmd_type="QUERY",state):
		#EMPTY-/?

		return str_ans


	def SYERLST(self,cmd_type="WRITE"):
		#EMPTY


	def SYFAST(self,cmd_type="QUERY",state):
		#EMPTY-/?

		return str_ans


	def SYSTART(self,cmd_type="WRITE"):
		#EMPTY


	def SYSTATE(self,cmd_type="QUERY"):
		#EMPTY/?

		return str_ans


	def SYTEST(self,cmd_type="QUERY"):
		#EMPTY/?

		return str_ans


	def RCABORT(self,cmd_type="WRITE"):
		#EMPTY


	def RCD(self,cmd_type="WRITE",sensor="A"):
		#EMPTY


	def RCDIAGO(self,cmd_type="WRITE"):
		#EMPTY


	def RCDIAGT(self,cmd_type="QUERY",sensor="A",test):
		#EMPTY-/?

		return str_ans


	def RCTEST(self,cmd_type="WRITE",sensor="A"):
		#EMPTY


	def RCZERO(self,cmd_type="WRITE"):
		#EMPTY


