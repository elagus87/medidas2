# -----------------------------------------------------------------------
#                       IMPORTs SECTION

from flask import Flask, render_template, jsonify, request

from numpy import linspace
from time import sleep

from tornado.ioloop import IOLoop
from threading import Thread

from bokeh.server.server import Server
from bokeh.application import Application
#from bokeh.application.handlers.function import FunctionHandler
from bokeh.resources import CDN
from bokeh.embed import components
from bokeh.models.sources import AjaxDataSource
from bokeh.plotting import figure, output_file, show
from bokeh.models import Select, Button, HoverTool, LabelSet, ColumnDataSource, Div, CrosshairTool
from bokeh.layouts import widgetbox, gridplot, row, column, layout


############################################################################3
# only for math functions
import math as m
from bokeh import events
from bokeh.models.callbacks import CustomJS
from scipy.signal import argrelextrema


#                       END IMPORTs SECTION
# -----------------------------------------------------------------------

# -----------------------------------------------------------------------
#                       CONFIG SECTION

SEVER_PORT = 8888

#                       END CONFIG SECTION
# -----------------------------------------------------------------------

# -----------------------------------------------------------------------
#                       GLOBAL VARs SECTION

pr = 0
start_freq = 0
stop_freq = 0
x = 0
y = 0

#                     END GLOBAL VARs SECTION
# -----------------------------------------------------------------------

print(">>>>>>>>>>>>> ENTRO GLOBAL")

app = Flask(__name__)

print(">>>>>>>>>>>>> ENTRO GLOBAL POST FLASK")

@app.route('/', methods=['GET', 'POST'])
def visualisation():
    print(">>>>>>>>>>>>> ENTRO VISUALISATION")
    plots = []
    plots.append(ajax_getplot())
    #plots2 = []
    #plots2.append(get_ajax_plot2())
    return render_template("dashboard.html",bokeh_css=CDN.render_css(),
                            bokeh_js=CDN.render_js(),plots=plots)#,plots2=plots2)


def ajax_getplot():
    global start_freq
    global stop_freq
    global x
    global y

    print(">>>>>>>>>>>>> ENTRO AJAX PLOT")

    source = AjaxDataSource(data_url = request.url_root + 'data/',
                            polling_interval = 1000, mode = 'replace')

    source.data = dict(x=[], y=[])

    plot = figure(plot_width = 400, plot_height = 400,tools="reset,undo,redo,save,box_zoom",toolbar_location="below")
    plot.sizing_mode = "stretch_width"
    plot.line('x', 'y', source = source, line_width = 4)
    plot.toolbar.active_drag = None
    hover = HoverTool(tooltips=[('frec','@x Hz'),('Y','@y dbm')],mode='vline', point_policy='snap_to_data') #='follow_mouse')
    hover.js_on_change
    plot.add_tools(hover)
    plot.add_tools(CrosshairTool(dimensions='height'))
    layout_all = layout(plot,sizing_mode='scale_width')

    plot = layout_all#l
    script, div = components(plot)
    return script, div


# def get_ajax_plot2():
#     source = AjaxDataSource(data_url = request.url_root + 'data/',
#                             polling_interval = 1000, mode = 'replace')

#     source.data = dict(x=[], y=[])
    

#     plot = figure(plot_width = 400, plot_height = 400,tools="reset,undo,redo,save,box_zoom",toolbar_location="below")
#     plot.sizing_mode = "stretch_width"
#     plot.line('x', 'y', source = source, line_width = 4)
#     plot.toolbar.active_drag = None
#     hover = HoverTool(tooltips=[('frec','@x Hz'),('Y','@y dbm')],mode='vline', point_policy='snap_to_data') #='follow_mouse')
#     hover.js_on_change
#     plot.add_tools(hover)
#     plot.add_tools(CrosshairTool(dimensions='height'))

#     script, div = components(plot)
#     return script, div


@app.route('/data/', methods=['POST'])
def data():
    global start_freq
    global stop_freq
    global pr
    global x
    global y

    # SE ENTRA TODOh EL TIEMPO, aproximadamente 1 vez cada polltime * cantidad de clientes
    # print(">>>>>>>>>>>>> ENTRO DATA") # == 127.0.0.1 - - [08/Nov/2019 12:37:33] "POST /data/ HTTP/1.1" 200 

    with open('./data.log', 'r') as f:
        
        # IF DATA IS READY TO BE READED, THEN: f.read(1)=="Y"
        do_read = False
        while do_read != True:
            f.seek(0)
            do_read = (f.read(1)=="Y")
            sleep(0.001)

        file_data = f.read().split(",")
        start_freq = float(file_data[1])
        stop_freq = float(file_data[2])
        lst_data = file_data[3:]
        lst_data[0]=lst_data[0][12:] # Discard first 12 elements
        readed = True

    if readed:
        y = list(map(float,lst_data))
        x = list(linspace(int(start_freq),int(stop_freq),len(lst_data)))
    
    return jsonify(x=x, y=y)



if __name__ == "__main__":

    print(">>>>>>>>>>>>> ENTRO MAIN")

    app.config["CACHE_TYPE"] = "null"

    app.run(host='0.0.0.0', port=SEVER_PORT, debug=False)

    print(">>>>>>>>>>>>> SALGO MAIN")
    
    print("Terminando la ejecución")
