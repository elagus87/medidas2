
var data = source.data;
var data2 = source2.data;
var data3 = source3.data;
var data4 = source4.data;
var data5 = source5.data;
var x_new = cb_obj.value - frec_start;
var step = cb_obj.step;
var y_new;
var idx;
var x2 = data2['x2'];
var y2 = data2['y2'];
var x3 = data3['x3'];
var y3 = data3['y3'];
var x4 = data4['x4'];
var y4 = data4['y4'];
var x5 = data5['x5'];
var y5 = data5['y5'];

if (Object.is(cb_obj, slider1)) {
    y_new = y[Math.round(x_new / step)];
    x2[0] = x_new;
    y2[0] = y_new;

    // actualizo valor en la tabla
    idx = data['x_data'].indexOf("Marcador 1");
    if (idx >= 0) {
        data['y_data'][idx] = y_new.toFixed(2).toString() + "dBm @ " + x_new.toFixed(2).toString() + " Hz";
    }
}

if (Object.is(cb_obj, slider2)) {
    y_new = y[Math.round(x_new / step)];
    x3[0] = x_new;
    y3[0] = y_new;

    // actualizo valor en la tabla
    idx = data['x_data'].indexOf("Marcador 2");
    if (idx >= 0) {
        data['y_data'][idx] = y_new.toFixed(2).toString() + "dBm @ " + x_new.toFixed(2).toString() + " Hz";
    }
}

if (Object.is(cb_obj, slider3)) {
    y_new = y[Math.round(x_new / step)];
    x4[0] = x_new;
    y4[0] = y_new;

    // actualizo valor en la tabla
    idx = data['x_data'].indexOf("Marcador 3");
    if (idx >= 0) {
        data['y_data'][idx] = y_new.toFixed(2).toString() + "dBm @ " + x_new.toFixed(2).toString() + " Hz";
    }
}

if (Object.is(cb_obj, slider4)) {
    y_new = y[Math.round(x_new / step)];
    x5[0] = x_new;
    y5[0] = y_new;

    // actualizo valor en la tabla
    idx = data['x_data'].indexOf("Marcador 4");
    if (idx >= 0) {
        data['y_data'][idx] = y_new.toFixed(2).toString() + "dBm @ " + x_new.toFixed(2).toString() + " Hz";
    }
}
source.change.emit();
source2.change.emit();
source3.change.emit();
source4.change.emit();
source5.change.emit();