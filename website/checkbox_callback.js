var data = source.data;
var maxEnable = Boolean(false);
var maxEnableRel = Boolean(false);
var idx = -1
var len;
var i;
l0.visible = false;
l1.visible = false;

// elimiino solo el maximo absoluto
do 
{
    idx = data['x_data'].indexOf("Maximo absoluto")
    if (idx >= 0) 
    {
        data['x_data'].splice(idx, 1)
        data['y_data'].splice(idx, 1)
    }
}
while (idx >= 0);

// elimiino solo los maximos relativos
do 
{
    idx = data['x_data'].indexOf("Maximo relativo")
    if (idx >= 0) 
    {
        data['x_data'].splice(idx, 1)
        data['y_data'].splice(idx, 1)
    }
}
while (idx >= 0);

checkbox.active.forEach(function (element) 
    {
        // EN CASO DE ACTIVARSE EL CHECK BOX
        switch (element) {
            case 0: // maximo abs
                maxEnable = true
                break;
            case 1: // maximos relativos
                maxEnableRel = true
                break;

            default:
                break;
        }
    });

if (maxEnable) 
{
    l0.visible = true;
    data['x_data'].push("Maximo absoluto")
    data['y_data'].push(max_abs)
}

if (maxEnableRel) 
{
    l1.visible = true;
    len = max_rel.length;
    for (i = 0; i < len; i++) {
        data['x_data'].push("Maximo relativo")
        data['y_data'].push(max_rel[i])
    }
}

source.change.emit();