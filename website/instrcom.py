# -*- coding: utf-8 -*-

import numpy as np
import pyvisa, time

#SEVER_PORT = 8888

ANALYZER_IP = "192.168.0.3"

central_frequency       = "99.9E6"
frequency_span          = "500E3"
bandwidth_resolution    = "1E3"
display_state           = "OFF" # "ON"
demodulator_state       = "OFF" # "ON"

isConnected = False

while isConnected == False:
    try:
        ResourcesManager = pyvisa.highlevel.ResourceManager()
        ResourceAnalizer = ResourcesManager.open_resource("TCPIP0::" + ANALYZER_IP + "::INSTR")
    except:
        print("Hubo un error al conectarse")
        time.sleep(1)
    else:
        print("La conexión fué exitosa")
        isConnected = True

# --------------------------------------------------------------------------------------------------------------
#                                   INITIAL ANALYZER CONFIG

ResourceAnalizer.write(":DISPlay:ENABle " + display_state)
ResourceAnalizer.write(":SENSe:BANDwidth:RESolution " + bandwidth_resolution)

ResourceAnalizer.write(":SENSe:FREQuency:CENTer " + central_frequency)
ResourceAnalizer.write(":SENSe:FREQuency:SPAN " + frequency_span)

ResourceAnalizer.write(":SENSe:DEMod " + demodulator_state)

#ResourceAnalizer.write(":SENSe:DEMod FM") #/AM/OFF
#ResourceAnalizer.write(":SENSe:GAIN:AUTO ON")
#ResourceAnalizer.write(":SENSe:DEMod:TIME 1ms")
# ResourceAnalizer.write(":SYSTem:SPEaker:STATe ON")

pr = ResourceAnalizer.query(":TRACe:DATA? TRACE1")

start_freq = int(ResourceAnalizer.query(":SENSe:FREQuency:STARt?").replace("\n,",""))
stop_freq = int(ResourceAnalizer.query(":SENSe:FREQuency:STOP?").replace("\n",""))
top_y = float(ResourceAnalizer.query(":DISPlay:WINdow:TRACe:Y:SCALe:RLEVel?").replace("\n",""))
bottom_y = top_y-10*float(ResourceAnalizer.query(":DISPlay:WINdow:TRACe:Y:SCALe:PDIVision?").replace("\n",""))

# --------------------------------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------------------------------
#                                   INITIAL GENEARATOR CONFIG

# --------------------------------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------------------------------
#                                   INITIAL POWERMETER CONFIG

# --------------------------------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------------------------------
#                                       CONTINOUS QUERIES

while True:
    try:
        analyzer_data = ResourceAnalizer.query(":TRACe:DATA? TRACE1")
        start_freq  = str(int(ResourceAnalizer.query(":SENSe:FREQuency:STARt?").replace("\n,","")))
        stop_freq   = str(int(ResourceAnalizer.query(":SENSe:FREQuency:STOP?").replace("\n","")))
        print("[INFO]   > ANALYZER: Data Queried")
    except:
        isConnected = False
        while isConnected == False:
            print("[ERROR]  > ANALYZER: Error when Querying")
            try:
                print("[ERROR]  > ANALYZER: Trying to open Resource")
                ResourceAnalizer = ResourcesManager.open_resource("TCPIP0::" + ANALYZER_IP + "::INSTR")
            except:
                print("[ERROR]  > ANALYZER: Error when Opening Resource")
            else:
                print("[ERROR]  > ANALYZER: Resource Successfully Opened")
                isConnected = True

            time.sleep(1)
    else:
        with open('data.log', 'w') as f:
            f.write("{:.1},{!s},{!s},{!s},{!s},{!s}".format("N",start_freq,stop_freq,bottom_y,top_y,analyzer_data))
            f.seek(0)
            f.write("{:.1}".format("Y"))
        
        time.sleep(1)

    # try:
    #     a 
    # except:
    #     a
    # else:
    #     with open('', 'r') as f:

        
    #     # with open('file.txt', 'r') as f:
    #     #     file_data = f.read().split(",")
    #     #     do_read = file_data[0]
    #     #     start = file_data[1]
    #     #     stop = file_data[2]
    #     #     data = file_data[3:]
    #     #     str_data = ','.join(data)
    #     #     str_data = str_data[12:]
    #     #     print("[INFO]   > "+str_data[:13])

    #     print("[INFO]   > Data Logged")

    #time.sleep(0.5)