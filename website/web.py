# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------
#                       IMPORTs SECTION

from flask import Flask, render_template, jsonify, request

from numpy import linspace
from time import sleep

from tornado.ioloop import IOLoop
from threading import Thread

from bokeh.resources import CDN
from widgets import ajax_getplot

#                       END IMPORTs SECTION
# -----------------------------------------------------------------------

# -----------------------------------------------------------------------
#                       CONFIG SECTION

SEVER_PORT = 8889

#                       END CONFIG SECTION
# -----------------------------------------------------------------------

# -----------------------------------------------------------------------
#                       GLOBAL VARs SECTION

start_freq = 0
stop_freq = 0
bottom_y = 0
top_y = 0
x = 0
y = 0

#                     END GLOBAL VARs SECTION
# -----------------------------------------------------------------------

print(">>>>>>>>>>>>> ENTRO GLOBAL")

app = Flask(__name__)

print(">>>>>>>>>>>>> ENTRO GLOBAL POST FLASK")

@app.route('/', methods=['GET', 'POST'])
def visualisation():
    global start_freq
    global stop_freq
    global bottom_y
    global top_y
    global x
    global y

    # PARECERÍA QUE SE ENTRA UNA ÚNICA VEZ POR CLIENTE
    print(">>>>>>>>>>>>> ENTRO VISUALISATION")
    plots = []
    plots.append(ajax_getplot(start_freq=start_freq,stop_freq=stop_freq,bottom_y=bottom_y,top_y=top_y,x=x,y=y))
    # #plots2 = []
    # #plots2.append(get_ajax_plot2())
    return render_template("dashboard.html",bokeh_css=CDN.render_css(),
                            bokeh_js=CDN.render_js(),plots=plots)#,plots2=plots2)


@app.route('/data/', methods=['POST'])
def data():
    global start_freq
    global stop_freq
    global bottom_y
    global top_y
    global x
    global y

    print(">>>>>>>>>>>>> ENTRO DATA")

    # DEPENDE DE LA CANTIDAD DE CLIENTES!!!
    # SE ENTRA TODOh EL TIEMPO, aproximadamente 1 vez cada polltime * cantidad de clientes
    # print(">>>>>>>>>>>>> ENTRO DATA") # == 127.0.0.1 - - [08/Nov/2019 12:37:33] "POST /data/ HTTP/1.1" 200 

    with open('./data.log', 'r') as f:
        
        # IF DATA IS READY TO BE READED, THEN: f.read(1)=="Y"
        do_read = False
        while do_read != True:
            f.seek(0)
            do_read = (f.read(1)=="Y")
            sleep(0.001)

        file_data   = f.read().split(",")
        start_freq  = float(file_data[1])
        stop_freq   = float(file_data[2])
        bottom_y    = float(file_data[3])
        top_y       = float(file_data[4])
        lst_data    = file_data[5:]
        lst_data[0] = lst_data[0][12:] # Discard first 12 elements
        readed = True

    if readed:
        y = list(map(float,lst_data))
        x = list(linspace(int(start_freq),int(stop_freq),len(lst_data)))
    
    return jsonify(x=x, y=y)



if __name__ == "__main__":

    print(">>>>>>>>>>>>> ENTRO MAIN")

    app.config["CACHE_TYPE"] = "null"

    app.run(host='127.0.0.1', port=SEVER_PORT, debug=False)

    print(">>>>>>>>>>>>> SALGO MAIN")
    
    print("Terminando la ejecucion")