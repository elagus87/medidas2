from flask import request

from numpy import linspace, asarray, pi, greater
from scipy import argmax
from time import sleep

from tornado.ioloop import IOLoop
from threading import Thread

from bokeh.server.server import Server
from bokeh.application import Application
#from bokeh.application.handlers.function import FunctionHandler
from bokeh.resources import CDN
from bokeh.embed import components
from bokeh.models.sources import AjaxDataSource
from bokeh.plotting import figure, output_file, show
from bokeh.models import Select, Button, HoverTool, LabelSet, ColumnDataSource, Div, CrosshairTool, TextInput, RadioButtonGroup
from bokeh.layouts import widgetbox, gridplot, row, column, layout
from bokeh.models.widgets import DataTable, DateFormatter, TableColumn, CheckboxGroup, Slider
#import math as m
#from bokeh import events
from bokeh.models.callbacks import CustomJS
from scipy.signal import argrelextrema

def ajax_getplot(start_freq,stop_freq,bottom_y,top_y,x,y):

    print(">>>>>>>>>>>>> ENTRO AJAX PLOT")

    source = AjaxDataSource(data_url = request.url_root + 'data/',
                            polling_interval = 1000, mode = 'replace')

    source.data = dict(x=[], y=[])

#################################################################################################################################################################
    # # --------------------------------------------------------------------------------------

    # button_height   = 30
    # button_width    = 70
    checkbox_width  = 100

    # --------------------------------------------------------------------------------------

    if x!=0 and y!=0:
        max_abs_y = max(y)
        max_abs_x = argmax(y)
        #x_npa=asarray(x)
        y_npa = asarray(y)
        x_axis = linspace(start_freq,stop_freq,len(x))
    else:
        #x_npa=asarray([0, 0])
        y_npa=asarray([0, 0])
        x_axis = [0, 0]
        max_abs_y = 0
        max_abs_x = 0

    ###############################################################################

    # maximos relativos
    max_rel_x = argrelextrema(y_npa, greater) #dato pasado a numpy array

    ###############################################################################

    # datos para la tabla
    info=[]
    value=[]

    # tabla de datos
    data = dict(x_data=info,y_data=value)
    source_table = ColumnDataSource(data)
    #source_table = source
    columns =   [ 
                    TableColumn(field="x_data", title="Informacion"),
                    TableColumn(field="y_data", title="Valor")
                ]

    function_sin = DataTable(source=source_table, columns=columns)#, width=400, height=200)

    #source_table = source

    ###############################################################################

    # En la ventana del plot
    graf = figure(
                    tools="reset,undo,redo,save,box_zoom",
                    title="Analizador de Espectro",
                    x_axis_label='Frecuencia [Hz]', y_axis_label='Potencia [dBm]',
                    sizing_mode = 'stretch_width',
                    plot_width=600, plot_height=300,
                    x_range=(start_freq, stop_freq),
                    y_range=(bottom_y, top_y)
                )

    props = dict(line_width = 4, line_alpha = 0.7)

    ###############################################################################
    # preparo el marcador movil

    # ve posibilidad de agregar PointDrawTool de hovertools
    # http://docs.bokeh.org/en/1.0.0/docs/user_guide/tools.html#click-tap-tools

    graf.add_tools(HoverTool(
        tooltips=[
            ( 'frec', '@x Hz'),
            ( 'amp',  '@y dBm' ), # use @{ } for field names with spaces
        ],
            point_policy='follow_mouse',
            mode='vline'
    ))

    ###############################################################################
    ## cosas a graficar

    source5 = ColumnDataSource(data=dict(x5=[x_axis[max_abs_x]], y5=[max_abs_y]))
    source4 = ColumnDataSource(data=dict(x4=[x_axis[max_abs_x]], y4=[max_abs_y]))
    source3 = ColumnDataSource(data=dict(x3=[x_axis[max_abs_x]], y3=[max_abs_y]))
    source2 = ColumnDataSource(data=dict(x2=[x_axis[max_abs_x]], y2=[max_abs_y]))

    # source5 = ColumnDataSource(data=dict(x5=[], y5=[]))
    # source4 = ColumnDataSource(data=dict(x4=[], y4=[]))
    # source3 = ColumnDataSource(data=dict(x3=[], y3=[]))
    # source2 = ColumnDataSource(data=dict(x2=[], y2=[]))
    
    # marcadores moviles que arrancan en el maximo absoluto
    l5=graf.circle('x5', 'y5', source=source5, color="lawngreen", line_width=8, line_alpha=0.7 )
    l4=graf.circle('x4', 'y4', source=source4, color="lime", line_width=8, line_alpha=0.7)
    l3=graf.circle('x3', 'y3', source=source3, color="yellow", line_width=8, line_alpha=0.7)
    l2=graf.circle('x2', 'y2', source=source2, color="blue", line_width=8, line_alpha=0.7)
    # max rel
    #l1 = graf.circle('x2', 'y2', source=source2, color="blue", line_width=8, line_alpha=0.7)
    l1 = graf.circle (x_axis [0] , y_npa [0], color = "yellowgreen", **props)
    # max
    #l0 = graf.circle('x2', 'y2', source=source2, color="blue", line_width=8, line_alpha=0.7)
    l0 = graf.circle (x_axis [max_abs_x], max_abs_y, color = "green", **props)

    graf.line('x', 'y', source = source, line_color="red")

    ###############################################################################
    # presentacion del maximo
    maximo=str('%.2f' % max_abs_y)+"V @ "+str('%.2f' % x_axis[max_abs_x]+" rad")

    # presentacion de maximos relativos
    max_rel=["a" for i in range(len(max_rel_x[0]))]

    for i in range(len((max_rel_x[0]))):
        max_rel[i]=(str('%.2f' % y_npa[max_rel_x[0][i]])+"V @ "+str('%.2f' % x_axis[max_rel_x[0][i]]+" rad"))

    ###############################################################################
    # Sliders de marcadores

    # solicitar frecuencias al analizador
    if start_freq!=stop_freq:

        frec_start  = start_freq   #x_axis[0]
        frec_stop   = stop_freq     #x_axis[-1]
        #step=(frec_stop-frec_start)/len(x_axis)

        if x != 0:
            step = (stop_freq-start_freq)/len(x)
            #print("-----------> "+str(step)+" -- "+str(len(x)))
            #print("-----------> "+str(x[10]-x[9]))
        else:
            step = 1

        init=(max_abs_x * step) + frec_start

        slider_m1 = Slider(start=frec_start, end=frec_stop, value=init, step=step, title="MARKER 1", width=900)
        slider_m2 = Slider(start=frec_start, end=frec_stop, value=init, step=step, title="MARKER 2", width=900)
        slider_m3 = Slider(start=frec_start, end=frec_stop, value=init, step=step, title="MARKER 3", width=900)
        slider_m4 = Slider(start=frec_start, end=frec_stop, value=init, step=step, title="MARKER 4", width=900)
        
    else:
        frec_start = 0
        frec_stop = 1
        step = 1
        init = 0
        slider_m1 = Slider(start=frec_start, end=frec_stop, value=init, step=step, title="MARKER 1", width=900)
        slider_m2 = Slider(start=frec_start, end=frec_stop, value=init, step=step, title="MARKER 2", width=900)
        slider_m3 = Slider(start=frec_start, end=frec_stop, value=init, step=step, title="MARKER 3", width=900)
        slider_m4 = Slider(start=frec_start, end=frec_stop, value=init, step=step, title="MARKER 4", width=900)

    #callback unico para todos los sliders
    with open("callback_sm.js","r") as f:
        callback_sm_code=f.read()

    callback_sm = CustomJS(args=dict(source5=source5,source4=source4,source3=source3,source2=source2, 
                                    slider1=slider_m1,slider2=slider_m2,slider3=slider_m3,slider4=slider_m4,
                                    graf=graf,props=props, l2=l2,y=y, frec_start=frec_start, source=source_table),code=callback_sm_code)

    slider_m1.js_on_change('value', callback_sm)
    slider_m2.js_on_change('value', callback_sm)
    slider_m3.js_on_change('value', callback_sm)
    slider_m4.js_on_change('value', callback_sm)

    ###############################################################################

    # Acciones relativas a los checkbox
    checkbox = CheckboxGroup(labels=["Max. Abs", "Max. Rel"], active=[], width=checkbox_width)

    checkbox_mark = CheckboxGroup(labels=["Mark 1", "Mark 2", "Mark 3", "Mark 4"], active=[], width=checkbox_width)

    #checkbox.active=[] <- indica los índices de los elementos "activados" (para activar el 1: [1], para activar el 0 y el 3: [0 3])
    #checkbox.active=[]
    #checkbox.active[0]=False
    l0.visible=False
    #checkbox.active[1]=False
    l1.visible=False

    #checkbox_mark.active=[]
    #checkbox_mark.active[0]=False
    l2.visible=False
    #checkbox_mark.active[1]=False
    l3.visible=False
    #checkbox_mark.active[2]=False
    l4.visible=False
    #checkbox_mark.active[3]=False
    l5.visible=False
    ##---------------------------------------------------------------------------##
    with open("checkbox_callback.js","r") as f:
        checkbox_code=f.read()

    checkbox.callback = CustomJS(args=dict(l0=l0, l1=l1, checkbox=checkbox,
                                max_abs=maximo,max_rel=max_rel,source=source_table,
                                ),code=checkbox_code)

    ##---------------------------------------------------------------------------##
    with open("checkbox_mark_callback.js","r") as f:
        checkbox_mark_code=f.read()

    checkbox_mark.callback = CustomJS(args=dict(l0=l0, l1=l1, l2=l2, l3=l3, l4=l4, l5=l5, checkbox=checkbox_mark,
                                max_abs=maximo,source=source_table,
                                slider1=slider_m1,slider2=slider_m2,slider3=slider_m3,slider4=slider_m4,
                                y=y, frec_start=frec_start),code=checkbox_mark_code)

    ##---------------------------------------------------------------------------##

    ###############################################################################
    # Seteos a enviar e informacion actual

    # textos informativos y titulos
    #set_div=Div(text="<b>Seteos del analizador</b>",width=500, height=10)
    mark_div=Div(text="<b>Marcadores e informacion del Espectro</b>")
    # x_div=Div(text="X Scale",width=30, height=10)
    # y_div=Div(text="Y Scale",width=30, height=10)
    # demod_div=Div(text="Demod",width=30, height=10)

    # test_div=Div(text="test",width=30, height=10)
    # reset_div=Div(text=" ",width=30, height=10)

    # # selectores
    # x_scale = RadioButtonGroup(labels=["Lin", "Log"], active=0)
    # y_scale = RadioButtonGroup(labels=["Lin", "Log"], active=1)

    # #demod_on = RadioButtonGroup(labels=["On", "Off"], active=1)
    # #demod_type = RadioButtonGroup(labels=["AM", "FM"], active=1)
    # demod_on = RadioButtonGroup(labels=["OFF","AM", "FM"], active=1)

    # cuadros de texto
    # send_freci = TextInput(value=" ", title="Frecuencia Inicial")
    # send_frecf = TextInput(value=" ", title="Frecuencia Final")
    # send_frecc = TextInput(value=" ", title="Frecuencia Central")
    # send_span = TextInput(value=" ", title="Span")
    # send_refl = TextInput(value=" ", title="Ref. Level")
    # send_demodt = TextInput(value=" ", title="Tiempo Demod.")

    # botones con sus callbacks
    # def cb_enviar(div=test_div,send_freci=send_freci,send_frecf=send_frecf,send_frecc=send_frecc,
    #             send_span=send_span,send_refl=send_refl,send_demodt=send_demodt,
    #             x_scale=x_scale,y_scale=y_scale,demod_on=demod_on): #,demod_type=demod_type):
    
    
    #     st_xscale="Log"     if (x_scale.active==1) else "Lin"
    #     st_yscale="Log"     if (y_scale.active==1) else "Lin"

    #     #st_demod_on="ON"    if (demod_on.active==0) else "OFF"
    #     #st_demod_type="AM"  if (demod_type.active==0) else "FM"
    #     if(demod_on.active==0):
    #         st_demod_on="OFF"
    #     elif(demod_on.active==1):
    #         st_demod_on="AM"
    #     else:
    #         st_demod_on="FM"
        
        # ### a partir de aca, van las funciones de envio
        # div.text=str(send_freci.value)+"\n"+str(send_frecc.value)+"\n" \
        #         +str(send_frecf.value)+"\n"+str(send_span.value)+"\n"+str(send_refl.value)+"\n" \
        #         +str(send_demodt.value)+"\n"+st_xscale+"\n" \
        #         +st_yscale+"\n"+st_demod_on+"\n" #+st_demod_type

    # send_bt = Button(label="Enviar",width=button_width, height=button_height)#,callback=CustomJS.from_py_func(cb_enviar))


    # # def cb_reset(div=reset_div):
    # #     div.text="Reset"
    # # poner envios de reset aqui !!!

    # send_reset = Button(label="Reset",width=button_width, height=button_height)#,callback=CustomJS.from_py_func(cb_reset))

    # # tabla de valores seteados actualmente
    # set_actual = dict(
    #         configuracion=["Frecuencia Inicial [Hz]","Frecuencia Central [Hz]","FrecuenciaFinal [Hz]","Span [Hz]","Ref. Level [dBm]","T. Demodulacion [s]"],
    #         valor=[start_freq,((stop_freq-start_freq)/2),stop_freq,(stop_freq-start_freq),0,0.1],
    #     )
    # source_set = ColumnDataSource(set_actual)

    # columns_set = [
    #         TableColumn(field="configuracion", title="Configuracion"),
    #         TableColumn(field="valor", title="valor"),
    #     ]
    #info_table = DataTable(source=source_set, columns=columns_set, width=400, height=280)

    # estructura de ordenamiento
    #send_col=column(send_freci,send_frecc,send_frecf,send_span,send_refl,send_demodt)
    #opc_col=column(x_div,x_scale,y_div,y_scale,demod_div,demod_on,send_bt,test_div,send_reset,reset_div)
    #info_col=column(info_table)

    ###############################################################################
    sliders=column(slider_m1,slider_m2,slider_m3,slider_m4)

    # Ploteos  
    layout_graf = gridplot([[graf]],sizing_mode='scale_width',toolbar_location="right")
    layout_widgets = widgetbox( row(mark_div),
                                row(checkbox,checkbox_mark,sliders,function_sin)
                            )

    # layout_widgets = widgetbox( row(mark_div),
    #                             row(checkbox,checkbox_mark,slider,function_sin),
    #                             row(set_div),
    #                             row(send_col,opc_col,info_col)
    #                         )

    l = layout([layout_graf,layout_widgets],sizing_mode='scale_width')
    #show(l)
    #l = layout_graf


#################################################################################################################################################################

    plot = l
    script, div = components(plot)
    return script, div